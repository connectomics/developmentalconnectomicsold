%% SUPPLEMENTARY FIGURE 2 - 
%Gour et al DevConn
%% Fig.S2B Target proximity analysis
%[cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_09-07-2020.xlsx','Sheet5');
[cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_16-07-2020.xlsx','Sheet5');
P7prox = cx_data((1:21),(1:3));
P7Inn = cx_data((1:21),10);
P9aprox = cx_data((24:53),(1:3));
P9aInn= cx_data((24:53),10);
P9bprox = cx_data((56:77),(1:3));
P9bInn = cx_data((56:77),10);
P9prox = cat(1,P9aprox,P9bprox);
P9Inn = cat(1,P9aInn,P9bInn);


figure
%for 1um
subplot(1,3,1)
hold on
scatter(P7prox(:,1),P7Inn,'xg','SizeData',70)
scatter(P9prox(:,1),P9Inn,'xy','SizeData',70)
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 1])
title('radius thr = 1um')
xlabel('fractional axon length in proximity')
ylabel('Soma preference')
daspect([1 1 1])

%for 3um
subplot(1,3,2)
hold on
scatter(P7prox(:,2),P7Inn,'xg','SizeData',70)
scatter(P9prox(:,2),P9Inn,'xy','SizeData',70)
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 1])
title('radius thr = 3um')
xlabel('fractional axon length in proximity')
ylabel('Soma preference')
daspect([1 1 1])

%for 5um
subplot(1,3,3)
hold on
scatter(P7prox(:,3),P7Inn,'xg','SizeData',70)
scatter(P9prox(:,3),P9Inn,'xy','SizeData',70)
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 1])
title('radius thr = 5um')
xlabel('fractional axon length in proximity')
ylabel('Soma preference')
daspect([1 1 1])
%% FigS2C - volumetric synapse density
clear all
% [cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_09-07-2020.xlsx','Sheet6');
[cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_16-07-2020.xlsx','Sheet6');
P7 = cx_data((1:2),13);
P9 = cx_data((3:7),13);
Mean = [mean(P7) mean(P9)];
g1 = ones(size(P9));

figure
boxplot(P9,g1,'Positions',2,'Colors','k')
hold on
plot([1 2],Mean,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
scatter(ones(numel(P7),1)*1+rand(numel(P7),1)*.25-.125, P7,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.005);
scatter(ones(numel(P9),1)*2+rand(numel(P9),1)*.25-.125, P9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.005);
box off
set(gca,'Ylim',[0 0.3])
set(gca,'Xlim',[0 3])
set(gca,'Xtick',[0 1 2])
set(gca,'Xticklabel',{'','7','9'})
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Volumetric synapse density')
xlabel('Postnatal Age (days)')

%% FigS2D - Hstogram of soma innervation perference of soma-seeded axons at P7 P9
clear all
cx_path = 'Z:\Data\goura\FigUpdates_Revision\RawData_NML\';
% [cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_09-07-2020.xlsx'));
[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'));

%P7 soma axons
ToSel_1 = find(cx_data(:,1)== 7.1& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_7 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1);
%P9a and P9b soma axons
ToSel_2a = find(cx_data(:,1)== 9& cx_data(:,3)== 3);
ToSel_2b = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
SMInn_9 = (cx_data(ToSel_2,9)-1)./(cx_data(ToSel_2,6)-1); 


P7 = histcounts(SMInn_7,0:0.05:1);
P9 = histcounts(SMInn_9,0:0.05:1);
figure;
hold on
bar([0:0.05:1]',[P7';P7(end)],'k');
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 20])
title('P7 soma-seeded axons')

figure;
hold on
bar([0:0.05:1]',[P9';P9(end)],'k');
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 20])
title('P9 soma-seeded axons')

