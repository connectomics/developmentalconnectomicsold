%% SUPPLEMENTARY FIGURE 3 
%Gour et al DevConn
%% multiplicity of innervation - AD axons
% load .mat file for AD axons ('Multiplicity_ADax_15-06-2020.mat')
figure
subplot(2,2,1)
scatter(ADP7(:,1),t7(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per AD (per axon)')
title('P7')

subplot(2,2,2)
scatter(ADP9(:,1),t9(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per AD (per axon)')
title('P9')

subplot(2,2,3)
scatter(ADP14(:,1),t14(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per AD (per axon)')
title('P14')

subplot(2,2,4)
scatter(ADP28(:,1),t28(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per AD (per axon)')
title('P28')

%% multiplicity for soma axons
% load .mat files ('Multiplicity_Smax_15-06-2020.mat')
figure
subplot(2,2,1)
scatter(SM7(:,1),t7(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P7')
 
subplot(2,2,2)
scatter(SM9(:,1),t9(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P9')
 
subplot(2,2,3)
scatter(SM14(:,1),t14(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P14')
 
subplot(2,2,4)
scatter(SM28(:,1),t28(:,1),'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P28')
