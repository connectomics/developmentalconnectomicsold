%% FIGURE_02 - L4 Connectomes
%P7 data
clear all
[cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_16-07-2020.xlsx','Sheet7');

%P7data
ToSel_1 = find(cx_data(:,1)== 7);
data7 = cx_data(ToSel_1,3:171);
%Sorting by syn counts
[x1,y1] = find(data7==1); % for 1 syn BLUE
[x2,y2] = find(data7==2); % for 2 syn LIGHT BLUE
[x3,y3] = find(data7==3); % for 3 syn CYAN
[x4,y4] = find(data7==4); % for 4 syn BET CYAN GREEN
[x5,y5] = find(data7==5); % 5 syn GREEN
[x7,y7] = find(data7>5 & data7<10); % 5 - 10 syn GREEN YELLOW
[x10,y10] = find(data7>=10 & data7 < 50); %10 to 50 YELLO
[x50,y50] = find(data7>=50 & data7 < 100); % 50 to 100 ORANGE
[x100,y100] = find(data7>=100); % >100 syn RED

load('Z:\Data\goura\FigureUpdates\Connectomes\AG_Colormap.mat');

figure;
scatter(x1,y1,'o','filled','MarkerEdgeColor',AG_cmap(1,:),'MarkerFaceColor',AG_cmap(1,:),'SizeData',8);
hold on
scatter(x2,y2,'o','filled','MarkerEdgeColor',AG_cmap(2,:),'MarkerFaceColor',AG_cmap(2,:),'SizeData',8);
scatter(x3,y3,'o','filled','MarkerEdgeColor',AG_cmap(3,:),'MarkerFaceColor',AG_cmap(3,:),'SizeData',8);
scatter(x4,y4,'o','filled','MarkerEdgeColor',AG_cmap(4,:),'MarkerFaceColor',AG_cmap(4,:),'SizeData',8);
scatter(x5,y5,'o','filled','MarkerEdgeColor',AG_cmap(5,:),'MarkerFaceColor',AG_cmap(5,:),'SizeData',8);
scatter(x7,y7,'o','filled','MarkerEdgeColor',AG_cmap(6,:),'MarkerFaceColor',AG_cmap(6,:),'SizeData',8);
scatter(x10,y10,'o','filled','MarkerEdgeColor',AG_cmap(7,:),'MarkerFaceColor',AG_cmap(7,:),'SizeData',8);
scatter(x50,y50,'o','filled','MarkerEdgeColor',AG_cmap(8,:),'MarkerFaceColor',AG_cmap(8,:),'SizeData',8);
scatter(x100,y100,'o','filled','MarkerEdgeColor',AG_cmap(9,:),'MarkerFaceColor',AG_cmap(9,:),'SizeData',8);
set(gca,'TickDir','out')
set(gca,'Color','k')
set(gca,'YAxisLocation','right')
set(gca,'Ylim',[0 (length(data7)+1)]);
set(gca,'Xlim',[0.5 (numel(data7(:,1))+0.5)]);
camroll(-90)
ylabel('Postsynaptic targets')
xlabel('Presynaptic axons')
title('P7 L4 - Conectome')


%P9data
ToSel_2 = find(cx_data(:,1)== 9);
data9 = cx_data(ToSel_2,3:142);
%Sorting by syn counts
[x1,y1] = find(data9==1); % for 1 syn BLUE
[x2,y2] = find(data9==2); % for 2 syn LIGHT BLUE
[x3,y3] = find(data9==3); % for 3 syn CYAN
[x4,y4] = find(data9==4); % for 4 syn BET CYAN GREEN
[x5,y5] = find(data9==5); % 5 syn GREEN
[x7,y7] = find(data9>5 & data9<10); % 5 - 10 syn GREEN YELLOW
[x10,y10] = find(data9>=10 & data9 < 50); %10 to 50 YELLO
[x50,y50] = find(data9>=50 & data9 < 100); % 50 to 100 ORANGE
[x100,y100] = find(data9>=100); % >100 syn RED
load('Z:\Data\goura\FigureUpdates\Connectomes\AG_Colormap.mat');
figure;
scatter(x1,y1,'o','filled','MarkerEdgeColor',AG_cmap(1,:),'MarkerFaceColor',AG_cmap(1,:),'SizeData',8);
hold on
scatter(x2,y2,'o','filled','MarkerEdgeColor',AG_cmap(2,:),'MarkerFaceColor',AG_cmap(2,:),'SizeData',8);
scatter(x3,y3,'o','filled','MarkerEdgeColor',AG_cmap(3,:),'MarkerFaceColor',AG_cmap(3,:),'SizeData',8);
scatter(x4,y4,'o','filled','MarkerEdgeColor',AG_cmap(4,:),'MarkerFaceColor',AG_cmap(4,:),'SizeData',8);
scatter(x5,y5,'o','filled','MarkerEdgeColor',AG_cmap(5,:),'MarkerFaceColor',AG_cmap(5,:),'SizeData',8);
scatter(x7,y7,'o','filled','MarkerEdgeColor',AG_cmap(6,:),'MarkerFaceColor',AG_cmap(6,:),'SizeData',8);
scatter(x10,y10,'o','filled','MarkerEdgeColor',AG_cmap(7,:),'MarkerFaceColor',AG_cmap(7,:),'SizeData',8);
scatter(x50,y50,'o','filled','MarkerEdgeColor',AG_cmap(8,:),'MarkerFaceColor',AG_cmap(8,:),'SizeData',8);
scatter(x100,y100,'o','filled','MarkerEdgeColor',AG_cmap(9,:),'MarkerFaceColor',AG_cmap(9,:),'SizeData',8);
set(gca,'TickDir','out')
set(gca,'Color','k')
set(gca,'YAxisLocation','right')
set(gca,'Ylim',[0 (length(data9)+1)]);
set(gca,'Xlim',[0.5 (numel(data9(:,1))+0.5)]);
camroll(-90)
ylabel('Postsynaptic targets')
xlabel('Presynaptic axons')
title('P9 L4 - Conectome')

%P14data
ToSel_3 = find(cx_data(:,1)== 14);
data14 = cx_data(ToSel_3,3:214);
%Sorting by syn counts
[x1,y1] = find(data14==1); % for 1 syn BLUE
[x2,y2] = find(data14==2); % for 2 syn LIGHT BLUE
[x3,y3] = find(data14==3); % for 3 syn CYAN
[x4,y4] = find(data14==4); % for 4 syn BET CYAN GREEN
[x5,y5] = find(data14==5); % 5 syn GREEN
[x7,y7] = find(data14>5 & data14<10); % 5 - 10 syn GREEN YELLOW
[x10,y10] = find(data14>=10 & data14 < 50); %10 to 50 YELLO
[x50,y50] = find(data14>=50 & data14 < 100); % 50 to 100 ORANGE
[x100,y100] = find(data14>=100); % >100 syn RED
load('Z:\Data\goura\FigureUpdates\Connectomes\AG_Colormap.mat');
figure;
scatter(x1,y1,'o','filled','MarkerEdgeColor',AG_cmap(1,:),'MarkerFaceColor',AG_cmap(1,:),'SizeData',8);
hold on
scatter(x2,y2,'o','filled','MarkerEdgeColor',AG_cmap(2,:),'MarkerFaceColor',AG_cmap(2,:),'SizeData',8);
scatter(x3,y3,'o','filled','MarkerEdgeColor',AG_cmap(3,:),'MarkerFaceColor',AG_cmap(3,:),'SizeData',8);
scatter(x4,y4,'o','filled','MarkerEdgeColor',AG_cmap(4,:),'MarkerFaceColor',AG_cmap(4,:),'SizeData',8);
scatter(x5,y5,'o','filled','MarkerEdgeColor',AG_cmap(5,:),'MarkerFaceColor',AG_cmap(5,:),'SizeData',8);
scatter(x7,y7,'o','filled','MarkerEdgeColor',AG_cmap(6,:),'MarkerFaceColor',AG_cmap(6,:),'SizeData',8);
scatter(x10,y10,'o','filled','MarkerEdgeColor',AG_cmap(7,:),'MarkerFaceColor',AG_cmap(7,:),'SizeData',8);
scatter(x50,y50,'o','filled','MarkerEdgeColor',AG_cmap(8,:),'MarkerFaceColor',AG_cmap(8,:),'SizeData',8);
scatter(x100,y100,'o','filled','MarkerEdgeColor',AG_cmap(9,:),'MarkerFaceColor',AG_cmap(9,:),'SizeData',8);
set(gca,'TickDir','out')
set(gca,'Color','k')
set(gca,'YAxisLocation','right')
set(gca,'Ylim',[0 (length(data14)+1)]);
set(gca,'Xlim',[0.5 (numel(data14(:,1))+0.5)]);
camroll(-90)
ylabel('Postsynaptic targets')
xlabel('Presynaptic axons')
title('P14 L4 - Conectome')

%P28data
ToSel_4 = find(cx_data(:,1)== 28);
data28 = cx_data(ToSel_4,3:172);
%Sorting by syn counts
[x1,y1] = find(data28==1); % for 1 syn BLUE
[x2,y2] = find(data28==2); % for 2 syn LIGHT BLUE
[x3,y3] = find(data28==3); % for 3 syn CYAN
[x4,y4] = find(data28==4); % for 4 syn BET CYAN GREEN
[x5,y5] = find(data28==5); % 5 syn GREEN
[x7,y7] = find(data28>5 & data28<10); % 5 - 10 syn GREEN YELLOW
[x10,y10] = find(data28>=10 & data28 < 50); %10 to 50 YELLO
[x50,y50] = find(data28>=50 & data28 < 100); % 50 to 100 ORANGE
[x100,y100] = find(data28>=100); % >100 syn RED
load('Z:\Data\goura\FigureUpdates\Connectomes\AG_Colormap.mat');
figure;
scatter(x1,y1,'o','filled','MarkerEdgeColor',AG_cmap(1,:),'MarkerFaceColor',AG_cmap(1,:),'SizeData',8);
hold on
scatter(x2,y2,'o','filled','MarkerEdgeColor',AG_cmap(2,:),'MarkerFaceColor',AG_cmap(2,:),'SizeData',8);
scatter(x3,y3,'o','filled','MarkerEdgeColor',AG_cmap(3,:),'MarkerFaceColor',AG_cmap(3,:),'SizeData',8);
scatter(x4,y4,'o','filled','MarkerEdgeColor',AG_cmap(4,:),'MarkerFaceColor',AG_cmap(4,:),'SizeData',8);
scatter(x5,y5,'o','filled','MarkerEdgeColor',AG_cmap(5,:),'MarkerFaceColor',AG_cmap(5,:),'SizeData',8);
scatter(x7,y7,'o','filled','MarkerEdgeColor',AG_cmap(6,:),'MarkerFaceColor',AG_cmap(6,:),'SizeData',8);
scatter(x10,y10,'o','filled','MarkerEdgeColor',AG_cmap(7,:),'MarkerFaceColor',AG_cmap(7,:),'SizeData',8);
scatter(x50,y50,'o','filled','MarkerEdgeColor',AG_cmap(8,:),'MarkerFaceColor',AG_cmap(8,:),'SizeData',8);
scatter(x100,y100,'o','filled','MarkerEdgeColor',AG_cmap(9,:),'MarkerFaceColor',AG_cmap(9,:),'SizeData',8);
set(gca,'TickDir','out')
set(gca,'Color','k')
set(gca,'YAxisLocation','right')
set(gca,'Ylim',[0 (length(data28)+1)]);
set(gca,'Xlim',[0.5 (numel(data28(:,1))+0.5)]);
camroll(-90)
ylabel('Postsynaptic targets')
xlabel('Presynaptic axons')
title('P28 L4 - Conectome')
%% BulkConnectomes
%for P7
cx_path = 'Z:\Data\goura\FigUpdates_Revision\RawData_NML\';
[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_09-07-2020.xlsx'));
cx_data(:,5)=[];

% for P7 AD axons
SynData = [];
Age = 7;
Layer = 4;
Seed = 1;
ToSel_1_AD = find(cx_data(:,1)== 7.1& cx_data(:,3)== 1);
SynData = cx_data(ToSel_1_AD,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));

ADBulk = (sum(ADSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);

BulkInn7_AD = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens7_AD = sum(TotalSyn)/sum(pL);
TotalLength7_AD = sum(pL);
nSyn7_AD = sum(TotalSyn);
P7AD = cat(2,Age,Layer,Seed,nSyn7_AD,BulkInn7_AD,TotalLength7_AD,AxCount);
Seed=2;
nSyn7_AIS = 0;
BulkInn7_AIS = zeros(size(BulkInn7_AD));
TotalLength7_AIS = 0;
AxCount=0;
P7AIS = cat(2,Age,Layer,Seed,nSyn7_AIS,BulkInn7_AIS,TotalLength7_AIS,AxCount);
%for P7Soma Axons
SynData=[];
Age = 7;
Layer = 4;
Seed = 3;
ToSel_1_SM = find(cx_data(:,1)== 7.1& cx_data(:,3)== 3);
SynData = cx_data(ToSel_1_SM,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));
ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);

BulkInn7_SM = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens7_SM = sum(TotalSyn)/sum(pL);
TotalLength7_SM = sum(pL);
nSyn7_SM = sum(TotalSyn);
P7SM = cat(2,Age,Layer,Seed,nSyn7_SM,BulkInn7_SM,TotalLength7_SM,AxCount);

% for P9 AD axons
Age = 9;
Layer = 4;
Seed = 1;
ToSel_1_ADa = find(cx_data(:,1)== 9& cx_data(:,3)== 1);
ToSel_1_ADb = find(cx_data(:,1)== 9.2& cx_data(:,3)== 1);
ToSel_1_AD = cat(1, ToSel_1_ADa, ToSel_1_ADb);
SynData = cx_data(ToSel_1_AD,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));

ADBulk = (sum(ADSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn9_AD = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens9_AD = sum(TotalSyn)/sum(pL);
TotalLength9_AD = sum(pL);
nSyn9_AD = sum(TotalSyn);
P9AD = cat(2,Age,Layer,Seed,nSyn9_AD,BulkInn9_AD,TotalLength9_AD,AxCount);

Seed = 2;
nSyn9_AIS = 0;
BulkInn9_AIS = zeros(size(BulkInn9_AD));
TotalLength9_AIS = 0;
AxCount = 0;
P9AIS = cat(2,Age,Layer,Seed,nSyn9_AIS,BulkInn9_AIS,TotalLength9_AIS,AxCount);

%for P9Soma Axons
SynData=[];
Age = 9;
Layer = 4;
Seed = 3;
ToSel_1_SMa = find(cx_data(:,1)== 9& cx_data(:,3)== 3);
ToSel_1_SMb = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3);
ToSel_1_SM = cat(1, ToSel_1_SMa, ToSel_1_SMb);
SynData = cx_data(ToSel_1_SM,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));

ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn9_SM = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens9_SM = sum(TotalSyn)/sum(pL);
TotalLength9_SM = sum(pL);
nSyn9_SM = sum(TotalSyn);
P9SM = cat(2,Age,Layer,Seed,nSyn9_SM,BulkInn9_SM,TotalLength9_SM,AxCount);
% % for P14 AD axons
Age = 14;
Layer = 4;
Seed = 1;
ToSel_1_ADa = find(cx_data(:,1)== 14.1& cx_data(:,3)== 1);
ToSel_1_ADb = find(cx_data(:,1)== 14.2& cx_data(:,3)== 1);
ToSel_1_AD = cat(1, ToSel_1_ADa, ToSel_1_ADb);
SynData = cx_data(ToSel_1_AD,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));
 
ADBulk = (sum(ADSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn14_AD = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens14_AD = sum(TotalSyn)/sum(pL);
TotalLength14_AD = sum(pL);
nSyn14_AD = sum(TotalSyn);
P14AD = cat(2,Age,Layer,Seed,nSyn14_AD,BulkInn14_AD,TotalLength14_AD,AxCount);

%for P14AIS Axons
SynData=[];
Age = 14;
Layer = 4;
Seed = 2;
ToSel_1_AISa = find(cx_data(:,1)== 14.1& cx_data(:,3)== 2);
ToSel_1_AISb = find(cx_data(:,1)== 14.2& cx_data(:,3)== 2);
ToSel_1_AIS = cat(1, ToSel_1_AISa, ToSel_1_AISb);
SynData = cx_data(ToSel_1_AIS,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));

ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn14_AIS = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens14_AIS = sum(TotalSyn)/sum(pL);
TotalLength14_AIS = sum(pL);
nSyn14_AIS = sum(TotalSyn);
P14AIS = cat(2,Age,Layer,Seed,nSyn14_AIS,BulkInn14_AIS,TotalLength14_AIS,AxCount);


%for P14Soma Axons
SynData=[];
Age = 14;
Layer = 4;
Seed = 3;
ToSel_1_SMa = find(cx_data(:,1)== 14.1& cx_data(:,3)== 3);
ToSel_1_SMb = find(cx_data(:,1)== 14.2& cx_data(:,3)== 3);
ToSel_1_SM = cat(1, ToSel_1_SMa, ToSel_1_SMb);
SynData = cx_data(ToSel_1_SM,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));

ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn14_SM = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens14_SM = sum(TotalSyn)/sum(pL);
TotalLength14_SM = sum(pL);
nSyn14_SM = sum(TotalSyn);
P14SM = cat(2,Age,Layer,Seed,nSyn14_SM,BulkInn14_SM,TotalLength14_SM,AxCount);

% for P28 AD axons
Age = 28;
Layer = 4;
Seed = 1;
ToSel_1_AD = find(cx_data(:,1)== 28& cx_data(:,3)== 1);
SynData = cx_data(ToSel_1_AD,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);
AxCount = numel(SynData(:,1));
 
ADBulk = (sum(ADSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn28_AD = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens28_AD = sum(TotalSyn)/sum(pL);
TotalLength28_AD = sum(pL);
nSyn28_AD = sum(TotalSyn);
P28AD = cat(2,Age,Layer,Seed,nSyn28_AD,BulkInn28_AD,TotalLength28_AD,AxCount);


%for P28AIS Axons
SynData=[];
Age = 28;
Layer = 4;
Seed = 2;
ToSel_1_AIS = find(cx_data(:,1)== 28& cx_data(:,3)== 2);
SynData = cx_data(ToSel_1_AIS,(4:12));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);

AxCount = numel(SynData(:,1));
ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn28_AIS = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens28_AIS = sum(TotalSyn)/sum(pL);
TotalLength28_AIS = sum(pL);
nSyn28_AIS = sum(TotalSyn);
P28AIS = cat(2,Age,Layer,Seed,nSyn28_AIS,BulkInn28_AIS,TotalLength28_AIS,AxCount);

%for P28Soma Axons
SynData=[];
Age = 28;
Layer = 4;
Seed = 3;
ToSel_1_SM = find(cx_data(:,1)== 28& cx_data(:,3)== 3);
SynData = cx_data(ToSel_1_SM,(4:12));
AxCount = numel(SynData(:,1));
ADSyn_num = SynData(:,3);
AISSyn_num = SynData(:,4);
SMSyn_num = SynData(:,5);
SpineSyn_num = SynData(:,6);
ShaftSyn_num = SynData(:,7);
GliaSyn_num = SynData(:,8);
SomFilSyn_num = SynData(:,9);
TotalSyn = SynData(:,2);
pL = SynData(:,1);

ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num))/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);
 
BulkInn28_SM = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);
BulkDens28_SM = sum(TotalSyn)/sum(pL);
TotalLength28_SM = sum(pL);
nSyn28_SM = sum(TotalSyn);
P28SM = cat(2,Age,Layer,Seed,nSyn28_SM,BulkInn28_SM,TotalLength28_SM,AxCount);

cx_dataN = cat(1,P7AD,P7AIS,P7SM,P9AD,P9AIS,...
P9SM,P14AD,P14AIS,P14SM,P28AD,P28AIS,P28SM);

% BULK CONNECTOMES

% cx_thisColmap = colormap(jet);%colormap(gray);
% cx_thisColmap(1,:) = [0 0 0];
load('Z:\Data\goura\FigUpdates_Revision\MatlabFigureCode\v02_14-07-2020\AG_ColorMap_BulkConnectome.mat')
cx_thisColmap=AG_ColorMap_BulkConnectome;
cx_postSel = [7 5 6]
cx_preSequ = [3 1 2]
cx_ageSequ = [7 9 14 28]
% cx_ageSequ = [9 14 28]
cx_structList = {'AD','AIS','soma','Spine','Shaft','Glia','Som. Fil.'}
figure
for i=1:length(cx_ageSequ)  
    i
    %subplot(5,1,i)
    figure
    cx_thisSel = find(cx_dataN(:,1)==cx_ageSequ(i));
    cx_thismx = cx_dataN(cx_thisSel,cx_postSel);
    cx_thismx(:,end+1) = 1-sum(cx_thismx,2);
    cx_thismx_show = (cx_thismx(cx_preSequ,:));
    cx_thismx_show(isinf(cx_thismx_show))=-5;
    cx_Clim = [-2 0];
    cx_thismx_show = cx_thismx(cx_preSequ,:);
    cx_Clim=[0 1];
    imagesc(cx_thismx_show);hold on;daspect([1 1 1]);
    set(gca,'CLim',cx_Clim);
%     set(gca,'ColorScale','log')
    colormap(cx_thisColmap);colorbar
    set(gca,'YTick',(1:length(cx_preSequ)),'YTickLabel',cx_structList(cx_dataN(cx_thisSel(cx_preSequ),3)),'XTick',[1:length(cx_postSel)+1],'XTickLabel',{cx_structList{cx_postSel-4},'rest'});
    title(sprintf('P%d, n= %d,%d,%d syn',cx_ageSequ(i),cx_dataN(cx_thisSel,4)));
end

