function waitForJob(job,interval)
    % Wait for job and display and update information about job every 60
    % seconds (default) or every x seconds (given by second input argument)
    if nargin < 2 || isempty(interval)
        interval = 60;
    end
    finished = false(length(job),1);
    for i=1:length(job)
        display(sprintf('Waiting for job %d: %s', job(i).Id, job(i).Name));
    end
    while ~all(finished)
        % Update status after 1 min wait
        pause(interval);
        for i=1:length(job)
            if ~finished(i)
                jobState = job(i).State;
                jobName = job(i).Name;
                errorCell = {job(i).Tasks(:).Error};
                stateCell = {job(i).Tasks(:).State};
                nrError = sum(~cellfun(@isempty, errorCell));
                nrFinished = sum(strcmp(stateCell, 'finished'));
                nrPending = sum(strcmp(stateCell, 'pending'));
                nrRunning = sum(strcmp(stateCell, 'running'));
                display(['[' datestr(clock) '] Job ' num2str(job(i).Id) ...
                    ' is ' jobState ', Tasks: ' ...
                    num2str(nrFinished) ' finished, ' ...
                    num2str(nrRunning) ' running, ' ...
                    num2str(nrPending) ' pending, ' ...
                    num2str(nrError) ' errors.']);
                if nrError ~= 0
                    taskIdx = find(~cellfun(@isempty, errorCell), 1, ...
                        'first');
                    fprintf(['At least one error occurred for job %d, ' ...
                        'task %d:'], job(i).Id, taskIdx);
                    getReport(errorCell{taskIdx})
                    error('Submitted job %d threw an error', job(i).Id);
                end
                if strcmp(jobState, 'finished')
                    finished(i) = true;
                    display(['Finished job: ' jobName]);
                end
            end
        end
    end

end