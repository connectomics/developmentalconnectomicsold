%% final supplement: control and reannotation results 2020-09-06 ff

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_syndata = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'));
cx_nbootstrp=20;
cx_bootstrpfrac=0.8;

cx_syndensAggr=[];
cx_densData = {}

figure

cx_rowranges = {[48:68],[69:82],[160:189],[190:211],[212:224]} % L4 som: p7d1 p7d2 p9d1 p9d2 p9d3
cx_plotpos_all = [1 1.3 2 2.3 2.6]
cx_subplots = [1 3]
cx_colrange = [9 13]    % som and som fil


for i=1:length(cx_rowranges)
    cx_thisrowrange = cx_rowranges{i}; 
    cx_plotpos = cx_plotpos_all(i);
    
    cx_thisdata = cx_syndata(cx_thisrowrange,:);
    cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,7:13),2)-1);
    cx_ns = sum(cx_thisdata(:,7:13),2);
    cx_pl = cx_thisdata(:,4);
    
    subplot(2,2,cx_subplots(1))
    plot(0.1*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xk');hold on
    plot(0.1*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[1 1 1]);hold on
    
    cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,7:13)))-size(cx_thisdata,1));
    plot(cx_plotpos+[-0.1 0.1],repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);
    cx_bootstrpmx=[];
    for ii=1:cx_nbootstrp
        cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
        cx_thissel = randperm(size(cx_thisdata,1));
        cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),7:13)))-cx_thisn);
        cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
    end
    %plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
    cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
    plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
    
    set(gca,'XLim',[0 3],'YLim',[-0.05 1],'TickDir','out'), box off
    
    subplot(2,2,cx_subplots(2))
    plot(0.1*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xk');hold on
    plot(0.1*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[1 1 1]);hold on
    
    cx_bulkavg = sum(sum(cx_thisdata(:,7:13)))/sum(sum(cx_pl));
    plot(cx_plotpos+[-0.1 0.1],repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);
    cx_bootstrpmx=[];
    for ii=1:cx_nbootstrp
        cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
        cx_thissel = randperm(size(cx_thisdata,1));
        cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),7:13))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
        cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
    end
    %plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
    cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
    plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
    
    set(gca,'XLim',[0 3],'YLim',[-0.05 0.5],'TickDir','out'), box off
    
    cx_syndensAggr(i,1)=cx_bulkavg;
    cx_syndensAggr(i,3)=cx_bootstrpsdev;
    cx_densData{i,1} = cx_ns./cx_pl;
    cx_densData{i,3} = cx_ns;
    cx_densData{i,4} = cx_pl;
end


cx_rowranges = {[14:33],[34:46],[84:133],[134:142],[143:158]} % L4 AD: p7d1 p7d2 p9d1 p9d2 p9d3
cx_plotpos_all = [1 1.3 2 2.3 2.6]
cx_subplots = [2 4]
cx_colrange = 7    % AD

for i=1:length(cx_rowranges)
    cx_thisrowrange = cx_rowranges{i}; 
    cx_plotpos = cx_plotpos_all(i);
    
    cx_thisdata = cx_syndata(cx_thisrowrange,:);
    cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,7:13),2)-1);
    cx_ns = sum(cx_thisdata(:,7:13),2);
    cx_pl = cx_thisdata(:,4);
    
    subplot(2,2,cx_subplots(1))
    plot(0.1*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xk');hold on
    plot(0.1*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[1 1 1]);hold on
    
    cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,7:13)))-size(cx_thisdata,1));
    plot(cx_plotpos+[-0.1 0.1],repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);
    cx_bootstrpmx=[];
    for ii=1:cx_nbootstrp
        cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
        cx_thissel = randperm(size(cx_thisdata,1));
        cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),7:13)))-cx_thisn);
        cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
    end
    %plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
    cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
    plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
    
    set(gca,'XLim',[0 3],'YLim',[-0.05 1],'TickDir','out'), box off
    
    subplot(2,2,cx_subplots(2))
    plot(0.1*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xk');hold on
    plot(0.1*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[1 1 1]);hold on
    
    cx_bulkavg = sum(sum(cx_thisdata(:,7:13)))/sum(sum(cx_pl));
    plot(cx_plotpos+[-0.1 0.1],repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);
    cx_bootstrpmx=[];
    for ii=1:cx_nbootstrp
        cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
        cx_thissel = randperm(size(cx_thisdata,1));
        cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),7:13))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
        cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
    end
    %plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
    cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
    plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
    
    set(gca,'XLim',[0 3],'YLim',[-0.05 0.5],'TickDir','out'), box off
    
    cx_syndensAggr(i,2)=cx_bulkavg;
    cx_syndensAggr(i,4)=cx_bootstrpsdev;
    cx_densData{i,2} = cx_ns./cx_pl;
    cx_densData{i,5} = cx_ns;
    cx_densData{i,6} = cx_pl;
end


%% add reannotations p7d1 and p9d1 som axons


%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_syndata = xlsread(fullfile(cx_path,'Reannotation_Workplan_sent_mh_v2_eval_v1_update_dataCollection2020_08_31_mh.xlsx'));


cx_thisrowrange = [1:3,6:13,15:21]; % p7 d1 som reann
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [20:22,24:25] % all syn incl seed, cl 1+2 not 3
cx_colrange = [20:22]   %soma incl seed, cl 1+2
cx_plotpos = 1.1
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
cx_pl = cx_thisdata(:,34);

subplot(2,2,1)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


subplot(2,2,3)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = sum(sum(cx_thisdata(:,cx_colrange_allsyn)))/sum(sum(cx_pl));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
cx_syndensAggr(6,1)=cx_bulkavg;
cx_syndensAggr(6,3)=cx_bootstrpsdev;
cx_densData{6,1} = cx_ns./cx_pl;
cx_densData{6,3} = cx_ns;
cx_densData{6,4} = cx_pl;

% p9d1


cx_thisrowrange = [57:64,66:72]; % p9 d1 som reann
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [20:22,24:25] % all syn incl seed, cl 1+2 not 3
cx_colrange = [20:22]   %soma incl seed cl 1+2
cx_plotpos = 2.1
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
cx_pl = cx_thisdata(:,34);

subplot(2,2,1)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


subplot(2,2,3)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = sum(sum(cx_thisdata(:,cx_colrange_allsyn)))/sum(sum(cx_pl));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
cx_syndensAggr(7,1)=cx_bulkavg;
cx_syndensAggr(7,3)=cx_bootstrpsdev;
cx_densData{7,1} = cx_ns./cx_pl;
cx_densData{7,3} = cx_ns;
cx_densData{7,4} = cx_pl;

%% add reannotations p7d1 and p9d1 AD axons


%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_syndata = xlsread(fullfile(cx_path,'Reannotation_Workplan_sent_mh_v2_eval_v1_update_dataCollection2020_09_09_mh_recheck.xlsx'));


cx_thisrowrange = [26:29,31:42]; % p7 d1 AD reann
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [20:22,24:25] % all syn incl seed, cl 1+2 not 3
cx_colrange = [20:22]   %AD incl seed, cl 1+2
cx_plotpos = 1.1
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
cx_pl = cx_thisdata(:,34);

subplot(2,2,2)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


subplot(2,2,4)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = sum(sum(cx_thisdata(:,cx_colrange_allsyn)))/sum(sum(cx_pl));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);

% p9d1


cx_thisrowrange = [93:113,115:124,126]; % p9 d1 AD reann
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [20:22,24:25] % all syn incl seed, cl 1+2 not 3
cx_colrange = [20:22]   %AD incl seed cl 1+2
cx_plotpos = 2.1
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
cx_pl = cx_thisdata(:,34);

subplot(2,2,2)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


subplot(2,2,4)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = sum(sum(cx_thisdata(:,cx_colrange_allsyn)))/sum(sum(cx_pl));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);



%% add reannotations p7d2 and p9d3 som axons (low res); Normalization to AD densities


%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_syndata = xlsread(fullfile(cx_path,'p7n2_p9n3_lowRes_reannotation_workplan_evalMH_wiCrosscheck.xlsx'));


cx_thisrowrange = [2:15]; % p7 d2 som reann
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [25:27,29:30]  % all syn, except cl 3
cx_colrange = [25:27]   %soma incl seed, cl 1+2
cx_plotpos = 1.4
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
cx_pl = cx_thisdata(:,39);

subplot(2,2,1)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


subplot(2,2,3)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = sum(sum(cx_thisdata(:,cx_colrange_allsyn)))/sum(sum(cx_pl));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);

cx_syndensAggr(8,1)=cx_bulkavg;
cx_syndensAggr(8,3)=cx_bootstrpsdev;
cx_densData{8,1} = cx_ns./cx_pl;
cx_densData{8,3} = cx_ns;
cx_densData{8,4} = cx_pl;

% p9 d3


cx_thisrowrange = [21:33]; % p9 d3 som reann
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [25:27,29:30]
cx_colrange = [25:27]   %soma incl seed cl 1+2
cx_plotpos = 2.7
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
cx_pl = cx_thisdata(:,39);

subplot(2,2,1)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


subplot(2,2,3)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = sum(sum(cx_thisdata(:,cx_colrange_allsyn)))/sum(sum(cx_pl));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
    cx_thissel = randperm(size(cx_thisdata,1));
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);


for i=1:4
    subplot(2,2,i)
    set(gca,'XTick',[1 1.1 1.3 1.4 2 2.1 2.3 2.6 2.7],'XTickLabel',{'p7d1','-c','p7d2','-c','p9d1','-c','p9d2','p9d3','-c'});
end
subplot(2,2,1),ylabel('p(soma|soma)');
subplot(2,2,2),ylabel('p(AD|AD)');
subplot(2,2,3),ylabel('syn dens (per um)');
subplot(2,2,4),ylabel('syn dens (per um)');

cx_syndensAggr(9,1)=cx_bulkavg;
cx_syndensAggr(9,3)=cx_bootstrpsdev;
cx_densData{9,1} = cx_ns./cx_pl;
cx_densData{9,3} = cx_ns;
cx_densData{9,4} = cx_pl;

% norm to AD dens

cx_syndensAggr(6:9,2) = cx_syndensAggr([1 3 2 5],2);
cx_syndensAggr_norm = cx_syndensAggr(:,1)./cx_syndensAggr(:,2)

% figure
% bar(cx_syndensAggr_norm([1 2 6 8 3 4 5 7 9]));

figure
plot(1,cx_densData{1,1}/cx_syndensAggr(1,2),'xm');hold on
plot(1,cx_syndensAggr_norm(1),'om');
plot(1+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(1),[1 2]),'m-');
plot([1 1],cx_syndensAggr_norm(1)+cx_syndensAggr(1,3)/cx_syndensAggr(1,2)*[+1 -1],'m-');

plot(1.1,cx_densData{6,1}/cx_syndensAggr(1,2),'xb');hold on
plot(1.1,cx_syndensAggr_norm(6),'ob');
plot(1.1+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(6),[1 2]),'m-');
plot([1 1]*1.1,cx_syndensAggr_norm(6)+cx_syndensAggr(6,3)/cx_syndensAggr(1,2)*[+1 -1],'m-');

plot(1.3,cx_densData{2,1}/cx_syndensAggr(2,2),'xm');hold on
plot(1.3,cx_syndensAggr_norm(2),'om');
plot(1.3+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(2),[1 2]),'m-');
plot([1 1]*1.3,cx_syndensAggr_norm(2)+cx_syndensAggr(2,3)/cx_syndensAggr(2,2)*[+1 -1],'m-');

plot(1.4,cx_densData{8,1}/cx_syndensAggr(2,2),'xb');hold on
plot(1.4,cx_syndensAggr_norm(8),'ob');
plot(1.4+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(8),[1 2]),'m-');
plot([1 1]*1.4,cx_syndensAggr_norm(8)+cx_syndensAggr(8,3)/cx_syndensAggr(2,2)*[+1 -1],'m-');

plot(2,cx_densData{3,1}/cx_syndensAggr(3,2),'xm');hold on
plot(2,cx_syndensAggr_norm(3),'om');
plot(2+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(3),[1 2]),'m-');
plot([1 1]*2,cx_syndensAggr_norm(3)+cx_syndensAggr(3,3)/cx_syndensAggr(3,2)*[+1 -1],'m-');

plot(2.1,cx_densData{7,1}/cx_syndensAggr(3,2),'xb');hold on
plot(2.1,cx_syndensAggr_norm(7),'ob');
plot(2.1+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(7),[1 2]),'m-');
plot([1 1]*2.1,cx_syndensAggr_norm(7)+cx_syndensAggr(7,3)/cx_syndensAggr(3,2)*[+1 -1],'m-');

plot(2.3,cx_densData{4,1}/cx_syndensAggr(4,2),'xm');hold on
plot(2.3,cx_syndensAggr_norm(4),'om');
plot(2.3+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(4),[1 2]),'m-');
plot([1 1]*2.3,cx_syndensAggr_norm(4)+cx_syndensAggr(4,3)/cx_syndensAggr(4,2)*[+1 -1],'m-');

plot(2.6,cx_densData{5,1}/cx_syndensAggr(5,2),'xm');hold on
plot(2.6,cx_syndensAggr_norm(5),'om');
plot(2.6+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(5),[1 2]),'m-');
plot([1 1]*2.6,cx_syndensAggr_norm(5)+cx_syndensAggr(5,3)/cx_syndensAggr(5,2)*[+1 -1],'m-');

plot(2.7,cx_densData{9,1}/cx_syndensAggr(5,2),'xb');hold on
plot(2.7,cx_syndensAggr_norm(9),'ob');
plot(2.7+[-0.1 0.1]*0.5,repmat(cx_syndensAggr_norm(9),[1 2]),'m-');
plot([1 1]*2.7,cx_syndensAggr_norm(9)+cx_syndensAggr(9,3)/cx_syndensAggr(5,2)*[+1 -1],'m-');

set(gca,'XLim',[0 3],'YLim',[0 5]);
ylabel('Syn density som axons (relative to AD axons)');
set(gca,'XTick',[1 1.1 1.3 1.4 2 2.1 2.3 2.6 2.7],'XTickLabel',{'p7d1','-c','p7d2','-c','p9d1','-c','p9d2','p9d3','-c'});


box off
set(gca,'TickDir','out');

cx_nbtstrp=1000

cx_str = 'p7d1 vs p9d1 original annotation'
cx_compdata1 = [cx_densData{1,3}/cx_syndensAggr(1,2),cx_densData{1,4}]; % 2= p7d1; AD dens norm
cx_compdata2 = [cx_densData{3,3}/cx_syndensAggr(3,2),cx_densData{3,4}]; % 2= p9d1; AD dens norm
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)

cx_str = 'p7d2 vs p9d3 new individuals control datasets, original annotation'
cx_compdata1 = [cx_densData{2,3}/cx_syndensAggr(2,2),cx_densData{2,4}]; % 2= p7d2; AD dens norm
cx_compdata2 = [cx_densData{5,3}/cx_syndensAggr(5,2),cx_densData{5,4}]; % 2= p9d3; AD dens norm
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)


cx_str = 'p7d1 vs p9d1+2 original annotation, data as in initial submission'
cx_compdata1 = [cx_densData{1,3}/cx_syndensAggr(1,2),cx_densData{1,4}]; % 2= p7d1; AD dens norm
cx_compdata2 = [[cx_densData{3,3}/cx_syndensAggr(3,2);cx_densData{4,3}/cx_syndensAggr(4,2)],[cx_densData{3,4};cx_densData{4,4}]]; % 2= p9d1 + d2; AD dens norm
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)

cx_str = 'p7d1 vs p9d1 RE-annotation by authors'
cx_compdata1 = [cx_densData{6,3}/cx_syndensAggr(1,2),cx_densData{6,4}]; % 2= p7d1 reann; AD dens norm
cx_compdata2 = [cx_densData{7,3}/cx_syndensAggr(3,2),cx_densData{7,4}]; % 2= p9d1 reann
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)

cx_str = 'p7d2 vs p9d3 RE-annotation by authors'
cx_compdata1 = [cx_densData{8,3}/cx_syndensAggr(2,2),cx_densData{8,4}]; % 2= p7d2 reann; AD dens norm
cx_compdata2 = [cx_densData{9,3}/cx_syndensAggr(5,2),cx_densData{9,4}]; % 2= p9d3 reann
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)

cx_str = 'p7d1 d2 vs p9d1+2+3 original annotation'
cx_compdata1 = [[cx_densData{1,3}/cx_syndensAggr(1,2);cx_densData{2,3}/cx_syndensAggr(2,2)],[cx_densData{1,4};cx_densData{2,4}]]; % p7d1+d2; AD dens norm
cx_compdata2 = [[cx_densData{3,3}/cx_syndensAggr(3,2);cx_densData{4,3}/cx_syndensAggr(4,2);cx_densData{5,3}/cx_syndensAggr(5,2)],[cx_densData{3,4};cx_densData{4,4};cx_densData{5,4}]]; % p9d1 + d2+d3; AD dens norm
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)

cx_str = 'p7d1c d2c vs p9d1c+d3c REannotation'
cx_compdata1 = [[cx_densData{6,3}/cx_syndensAggr(1,2);cx_densData{8,3}/cx_syndensAggr(2,2)],[cx_densData{6,4};cx_densData{8,4}]]; % p7d1+d2; AD dens norm
cx_compdata2 = [[cx_densData{7,3}/cx_syndensAggr(3,2);cx_densData{9,3}/cx_syndensAggr(5,2)],[cx_densData{7,4};cx_densData{9,4}]]; % p9d1 + d2+d3; AD dens norm
mhcode_bootstrapCompareFrac(cx_compdata1,cx_compdata2,cx_nbtstrp,cx_str)

mh_saveallfigs(fullfile(cx_path,'ControlFigure_Statistics'));

%% P5 data, including additional annotations by authors

%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_syndata = xlsread(fullfile(cx_path,'P5_L4_reannotation_inclNewAnnot.xlsx'));
cx_nbootstrp=1000;%20;
cx_bootstrpfrac=1;

cx_thisrowrange = [1:7]; % p5 AD
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [22:24,26:27]
cx_colrange = [22:24]   %AD incl seed cl 1+2
cx_plotpos = 0.5
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
%cx_pl = cx_thisdata(:,39);

subplot(2,2,1)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xb');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 0 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    if cx_bootstrpfrac<1
        cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
        cx_thissel = randperm(size(cx_thisdata,1));
    else
        cx_thisn=size(cx_thisdata,1);
        cx_thissel = randi(cx_thisn,1,cx_thisn);
    end
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);

set(gca,'YLim',[0 1]);

cx_savedata_ADp5 = cx_fractions;
cx_savedata_ADp5_btstrp = cx_bootstrpmx(:,2);
cx_savedata_ADp5_stat = [cx_bulkavg,cx_bootstrpsdev,cx_thisn];

% p5 som

cx_thisrowrange = [14:25]; % p5 som
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [22:24,26:27]
cx_colrange = [22:24]   %AD incl seed cl 1+2
cx_plotpos = 0.5
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
%cx_pl = cx_thisdata(:,39);

subplot(2,2,1)
plot(0.05*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xr');hold on
plot(0.05*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[0 1 1]);hold on

cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,cx_colrange_allsyn)))-size(cx_thisdata,1));
plot(cx_plotpos+[-0.1 0.1]*0.5,repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);

cx_bootstrpmx=[];
for ii=1:cx_nbootstrp
    if cx_bootstrpfrac<1
        cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
        cx_thissel = randperm(size(cx_thisdata,1));
    else
        cx_thisn=size(cx_thisdata,1);
        cx_thissel = randi(cx_thisn,1,cx_thisn);
    end
    cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange_allsyn)))-cx_thisn);
    cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
end
%plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);

cx_savedata_SOMp5 = cx_fractions;
cx_savedata_SOMp5_btstrp = cx_bootstrpmx(:,2);

cx_savedata_SOMp5_stat = [cx_bulkavg,cx_bootstrpsdev,cx_thisn];

[h,p] = ttest2(cx_savedata_ADp5,cx_savedata_SOMp5)
[h,p] = ranksum(cx_savedata_ADp5,cx_savedata_SOMp5)
[h,p] = kstest2(cx_savedata_ADp5,cx_savedata_SOMp5)




figure
subplot(1,2,1),hist(cx_savedata_SOMp5_btstrp);hold on;set(gca,'XLim',[0 1])
subplot(1,2,2),hist(cx_savedata_ADp5_btstrp);hold on;set(gca,'XLim',[0 1])


[t,p] = mhcode_welch(cx_savedata_ADp5_stat(1),cx_savedata_SOMp5_stat(1) ,cx_savedata_ADp5_stat(2),cx_savedata_SOMp5_stat(2) ,cx_savedata_ADp5_stat(3),cx_savedata_SOMp5_stat(3))


%% revisiting the bootstrap


%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_syndata = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'));
cx_nbootstrp=1000%20;
cx_bootstrpfrac=1;%1%0.8;


for cx_somAd =1:2

    cx_syndensAggr=[];
    cx_densData = {}
    
    xx_figref=figure
    xx_figref2 = figure
    
    
    % SOM version
    if cx_somAd==1
        cx_rowranges = {[48:68],[69:82],[160:189],[190:211],[212:224]} % L4 som: p7d1 p7d2 p9d1 p9d2 p9d3
        cx_plotpos_all = [1 1.3 2 2.3 2.6]
        cx_subplots = [1 3]
        cx_colrange = [9]% 13]    % som and som fil
    elseif cx_somAd==2
        
        % AD VERSION:
        cx_rowranges = {[14:33],[34:46],[84:133],[134:142],[143:158]} % L4 AD: p7d1 p7d2 p9d1 p9d2 p9d3
        cx_plotpos_all = [1 1.3 2 2.3 2.6]
        cx_subplots = [2 4]
        cx_colrange = 7    % AD
    end
    
    cx_datasave = {};
    
    for i=1:length(cx_rowranges)
        cx_thisrowrange = cx_rowranges{i};
        cx_plotpos = cx_plotpos_all(i);
        
        cx_thisdata = cx_syndata(cx_thisrowrange,:);
        cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,7:13),2)-1);
        cx_ns = sum(cx_thisdata(:,7:13),2);
        cx_pl = cx_thisdata(:,4);
        cx_datasave{i,1} = cx_fractions;
        cx_datasave{i,3} = size(cx_thisdata,1);
        
        figure(xx_figref),subplot(2,2,cx_subplots(1))
        plot(0.1*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_fractions(cx_ns>=10),'xk');hold on
        plot(0.1*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_fractions(cx_ns<10),'.','Color',0.5*[1 1 1]);hold on
        
        cx_bulkavg = (sum(sum(cx_thisdata(:,cx_colrange)))-size(cx_thisdata,1))/(sum(sum(cx_thisdata(:,7:13)))-size(cx_thisdata,1));
        plot(cx_plotpos+[-0.1 0.1],repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);
        cx_bootstrpmx=[];
        for ii=1:cx_nbootstrp
            if cx_bootstrpfrac<1
                cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
                cx_thissel = randperm(size(cx_thisdata,1));
            else
                cx_thisn=size(cx_thisdata,1);
                cx_thissel = randi(cx_thisn,1,cx_thisn);
            end
            cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),cx_colrange)))-cx_thisn)/(sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),7:13)))-cx_thisn);
            cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
        end
        %plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
        cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
        plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
        cx_datasave{i,4} = cx_bulkavg;cx_datasave{i,5} = cx_bootstrpsdev;
        
        set(gca,'XLim',[0 3],'YLim',[-0.05 1],'TickDir','out'), box off
        figure(xx_figref2),subplot(2,length(cx_rowranges),i)
        hist(cx_bootstrpmx(:,2)); set(gca,'XLim',[0 1]);
        
        figure(xx_figref),subplot(2,2,cx_subplots(2))
        plot(0.1*(rand(1,length(find(cx_ns>=10)))-0.5)+cx_plotpos,cx_ns(cx_ns>=10)./cx_pl(cx_ns>=10),'xk');hold on
        plot(0.1*(rand(1,length(find(cx_ns<10)))-0.5)+cx_plotpos,cx_ns(cx_ns<10)./cx_pl(cx_ns<10),'.','Color',0.5*[1 1 1]);hold on
        cx_datasave{i,2} = cx_ns./cx_pl;
        
        cx_bulkavg = sum(sum(cx_thisdata(:,7:13)))/sum(sum(cx_pl));
        plot(cx_plotpos+[-0.1 0.1],repmat(cx_bulkavg,[1 2]),'m-','MarkerSize',15);
        cx_bootstrpmx=[];
        for ii=1:cx_nbootstrp
            if cx_bootstrpfrac<1
                cx_thisn=floor(cx_bootstrpfrac*size(cx_thisdata,1));
                cx_thissel = randperm(size(cx_thisdata,1));
            else
                cx_thisn=size(cx_thisdata,1);
                cx_thissel = randi(cx_thisn,1,cx_thisn);
            end
            cx_thisavg = (sum(sum(cx_thisdata(cx_thissel(1:cx_thisn),7:13))))/(sum(sum(cx_pl(cx_thissel(1:cx_thisn)))));
            cx_bootstrpmx(ii,1:size(cx_thisdata,1)+2)=[cx_thisn,cx_thisavg,cx_thissel];
        end
        %plot(cx_plotpos,cx_bootstrpmx(:,2),'om');
        cx_bootstrpsdev = std(cx_bootstrpmx(:,2));
        plot([1 1]*cx_plotpos,[cx_bulkavg+cx_bootstrpsdev,cx_bulkavg-cx_bootstrpsdev],'m-','MarkerSize',15);
        
        set(gca,'XLim',[0 3],'YLim',[-0.05 0.5],'TickDir','out'), box off
        figure(xx_figref2),subplot(2,length(cx_rowranges),length(cx_rowranges)+i)
        hist(cx_bootstrpmx(:,2)); set(gca,'XLim',[0 1]);
        
        cx_datasave{i,6} = cx_bulkavg;cx_datasave{i,7} = cx_bootstrpsdev;
        
        cx_syndensAggr(i,1)=cx_bulkavg;
        cx_syndensAggr(i,3)=cx_bootstrpsdev;
        cx_densData{i,1} = cx_ns./cx_pl;
    end
    
    if cx_somAd==1
        cx_datasaveSOM = cx_datasave;
    elseif cx_somAd==2
        cx_datasaveAD = cx_datasave;
    end
    
    
    cx_datasave{1,2}
    cx_datasave{3,2}
    
    [h,p] = ttest2(cx_datasave{1,2},cx_datasave{3,2})
    [h,p] = ranksum(cx_datasave{1,2},cx_datasave{3,2})
    
    [h,p] = ttest2([cx_datasave{1,2};cx_datasave{2,2}],[cx_datasave{3,2};cx_datasave{4,2};cx_datasave{5,2}])
    [h,p] = ranksum([cx_datasave{1,2};cx_datasave{2,2}],[cx_datasave{3,2};cx_datasave{4,2};cx_datasave{5,2}])
    
    
    [h,p] = ttest2(cx_datasave{1,1},cx_datasave{3,1})
    [h,p] = ranksum(cx_datasave{1,1},cx_datasave{3,1})
    [h,p] = kstest2(cx_datasave{1,1},cx_datasave{3,1})
    
    [h,p] = ttest2(cx_datasave{1,1},[cx_datasave{3,1};cx_datasave{4,1}])
    [h,p] = ranksum(cx_datasave{1,1},[cx_datasave{3,1};cx_datasave{4,1}])
    [h,p] = kstest2(cx_datasave{1,1},[cx_datasave{3,1};cx_datasave{4,1}])
    
    
    [h,p] = ttest2([cx_datasave{1,1};cx_datasave{2,1}],[cx_datasave{3,1};cx_datasave{4,1};cx_datasave{5,1}])
    [h,p] = ranksum([cx_datasave{1,1};cx_datasave{2,1}],[cx_datasave{3,1};cx_datasave{4,1};cx_datasave{5,1}])
    [h,p] = kstest2([cx_datasave{1,1};cx_datasave{2,1}],[cx_datasave{3,1};cx_datasave{4,1};cx_datasave{5,1}])
    
    
    % compare p7,p9 fractions using Welch's t-test:
    cx_ij=[1 3]
    cx_n1 = cx_datasave{cx_ij(1),3};
    cx_n2 = cx_datasave{cx_ij(2),3};
    cx_scomb = sqrt(cx_datasave{cx_ij(1),5}^2+cx_datasave{cx_ij(2),5}^2)
    
    cx_t = abs(cx_datasave{cx_ij(1),4}-cx_datasave{cx_ij(2),4})/cx_scomb
    cx_df = (cx_datasave{cx_ij(1),5}^2+cx_datasave{cx_ij(2),5}^2)^2/...
        (cx_datasave{cx_ij(1),5}^4/(cx_n1-1) + cx_datasave{cx_ij(2),5}^4/(cx_n2-1))
    cx_p = 1-tcdf(cx_t,cx_df)
    
    [t,p] = mhcode_welch(cx_datasave{cx_ij(1),4},cx_datasave{cx_ij(2),4},cx_datasave{cx_ij(1),5},cx_datasave{cx_ij(2),5},cx_n1,cx_n2)
    
    % figure
    % plot([-10:10],tcdf(-10:10,cx_df))
    
    
    % compare p7,p9 syn dens using Welch's t-test:
    cx_ij=[1 3]
    cx_n1 = cx_datasave{cx_ij(1),3};
    cx_n2 = cx_datasave{cx_ij(2),3};
    cx_scomb = sqrt(cx_datasave{cx_ij(1),7}^2+cx_datasave{cx_ij(2),7}^2)
    
    cx_t = abs(cx_datasave{cx_ij(1),6}-cx_datasave{cx_ij(2),6})/cx_scomb
    cx_df = (cx_datasave{cx_ij(1),7}^2+cx_datasave{cx_ij(2),7}^2)^2/...
        (cx_datasave{cx_ij(1),7}^4/(cx_n1-1) + cx_datasave{cx_ij(2),7}^4/(cx_n2-1))
    cx_p = 1-tcdf(cx_t,cx_df)
end


%% plot combined p5-p7-p9 fractions Fig. 3E

% wi raw data points

cx_xw=0.4
cx_lw=5

figure
plot(5,cx_savedata_ADp5,'xc');hold on
plot(5+[-1 1]*cx_xw,[1 1]*cx_savedata_ADp5_stat(1),'-c','LineWidth',cx_lw);
plot(5*[1 1],cx_savedata_ADp5_stat(1)+[-1 1]*cx_savedata_ADp5_stat(2),'-c','LineWidth',cx_lw);

plot(5,cx_savedata_SOMp5,'xm');hold on
plot(5+[-1 1]*cx_xw,[1 1]*cx_savedata_SOMp5_stat(1),'-m','LineWidth',cx_lw);
plot(5*[1 1],cx_savedata_SOMp5_stat(1)+[-1 1]*cx_savedata_SOMp5_stat(2),'-m','LineWidth',cx_lw);


plot(7,cx_datasaveSOM{1,1},'xm');hold on
plot(7+[-1 1]*cx_xw,[1 1]*cx_datasaveSOM{1,4},'-m','LineWidth',cx_lw);
plot(7*[1 1],cx_datasaveSOM{1,4}+[-1 1]*cx_datasaveSOM{1,5},'-m','LineWidth',cx_lw);


plot(9,cx_datasaveSOM{3,1},'xm');hold on
plot(9+[-1 1]*cx_xw,[1 1]*cx_datasaveSOM{3,4},'-m','LineWidth',cx_lw);
plot(9*[1 1],cx_datasaveSOM{3,4}+[-1 1]*cx_datasaveSOM{3,5},'-m','LineWidth',cx_lw);


plot(7,cx_datasaveAD{1,1},'xm');hold on
plot(7+[-1 1]*cx_xw,[1 1]*cx_datasaveAD{1,4},'-c','LineWidth',cx_lw);
plot(7*[1 1],cx_datasaveAD{1,4}+[-1 1]*cx_datasaveAD{1,5},'-c','LineWidth',cx_lw);

plot(9,cx_datasaveAD{3,1},'xm');hold on
plot(9+[-1 1]*cx_xw,[1 1]*cx_datasaveAD{3,4},'-c','LineWidth',cx_lw);
plot(9*[1 1],cx_datasaveAD{3,4}+[-1 1]*cx_datasaveAD{3,5},'-c','LineWidth',cx_lw);

set(gca,'XLim',[0 10])


[t,p] = mhcode_welch(cx_savedata_ADp5_stat(1),cx_datasaveAD{1,4},cx_savedata_ADp5_stat(2),cx_datasaveAD{1,5},length(cx_savedata_ADp5),cx_datasaveAD{1,3})
[t,p] = mhcode_welch(cx_savedata_SOMp5_stat(1),cx_datasaveSOM{1,4},cx_savedata_SOMp5_stat(2),cx_datasaveSOM{1,5},length(cx_savedata_SOMp5),cx_datasaveSOM{1,3})

[h,p]= kstest2(cx_savedata_ADp5,cx_datasaveAD{1,1})
[h,p]= kstest2(cx_savedata_SOMp5,cx_datasaveSOM{1,1})

box off
set(gca,'TickDir','out')


% wi raw data points

cx_xw=0.2
cx_lw=5
cx_bulklines=[];

figure
%plot(5,cx_savedata_ADp5,'xc');hold on
%plot(5+[-1 1]*cx_xw,[1 1]*cx_savedata_ADp5_stat(1),'-c','LineWidth',cx_lw);hold on
%plot(5*[1 1],cx_savedata_ADp5_stat(1)+[-1 1]*cx_savedata_ADp5_stat(2),'-c','LineWidth',cx_lw);hold on
cx_bulklines(1,1)=cx_savedata_ADp5_stat(1);
cx_bulklines(2,1)=cx_savedata_ADp5_stat(2);

%plot(5,cx_savedata_SOMp5,'xm');hold on
%plot(5+[-1 1]*cx_xw,[1 1]*cx_savedata_SOMp5_stat(1),'-m','LineWidth',cx_lw);
%plot(5*[1 1],cx_savedata_SOMp5_stat(1)+[-1 1]*cx_savedata_SOMp5_stat(2),'-m','LineWidth',cx_lw);
cx_bulklines(3,1)=cx_savedata_SOMp5_stat(1);
cx_bulklines(4,1)=cx_savedata_SOMp5_stat(2);


%plot(7,cx_datasaveSOM{1,1},'xm');hold on
%plot(7+[-1 1]*cx_xw,[1 1]*cx_datasaveSOM{1,4},'-m','LineWidth',cx_lw);
% plot(7*[1 1],cx_datasaveSOM{1,4}+[-1 1]*cx_datasaveSOM{1,5},'-m','LineWidth',cx_lw);
cx_bulklines(3,2)=cx_datasaveSOM{1,4};
cx_bulklines(4,2)=cx_datasaveSOM{1,5};


%plot(9,cx_datasaveSOM{3,1},'xm');hold on
%plot(9+[-1 1]*cx_xw,[1 1]*cx_datasaveSOM{3,4},'-m','LineWidth',cx_lw);
% plot(9*[1 1],cx_datasaveSOM{3,4}+[-1 1]*cx_datasaveSOM{3,5},'-m','LineWidth',cx_lw);
cx_bulklines(3,3)=cx_datasaveSOM{3,4};
cx_bulklines(4,3)=cx_datasaveSOM{3,5};


%plot(7,cx_datasaveAD{1,1},'xm');hold on
%plot(7+[-1 1]*cx_xw,[1 1]*cx_datasaveAD{1,4},'-c','LineWidth',cx_lw);
% plot(7*[1 1],cx_datasaveAD{1,4}+[-1 1]*cx_datasaveAD{1,5},'-c','LineWidth',cx_lw);
cx_bulklines(1,2)=cx_datasaveAD{1,4};
cx_bulklines(2,2)=cx_datasaveAD{1,5};

%plot(9,cx_datasaveAD{3,1},'xm');hold on
%plot(9+[-1 1]*cx_xw,[1 1]*cx_datasaveAD{3,4},'-c','LineWidth',cx_lw);
% plot(9*[1 1],cx_datasaveAD{3,4}+[-1 1]*cx_datasaveAD{3,5},'-c','LineWidth',cx_lw);
cx_bulklines(1,3)=cx_datasaveAD{3,4};
cx_bulklines(2,3)=cx_datasaveAD{3,5};

cx_bulklines=cx_bulklines*100;

plot([5 7 9],cx_bulklines(1,:),'c-o','LineWidth',cx_lw);hold on
plot([5 7 9],cx_bulklines(1,:)+cx_bulklines(2,:),'c-','LineWidth',1);
plot([5 7 9],cx_bulklines(1,:)-cx_bulklines(2,:),'c-','LineWidth',1);

plot([5 7 9],cx_bulklines(3,:),'m-o','LineWidth',cx_lw);
plot([5 7 9],cx_bulklines(3,:)+cx_bulklines(4,:),'m-','LineWidth',1);
plot([5 7 9],cx_bulklines(3,:)-cx_bulklines(4,:),'m-','LineWidth',1);


set(gca,'XLim',[4 10]);set(gca,'XTick',[5 7 9],'YTick',[0:5:50],'YLim',[0 50]);xlabel('Age');ylabel('Fracitonal innervation of targets (%)');

[t,p] = mhcode_welch(cx_savedata_ADp5_stat(1),cx_datasaveAD{1,4},cx_savedata_ADp5_stat(2),cx_datasaveAD{1,5},length(cx_savedata_ADp5),cx_datasaveAD{1,3})
[t,p] = mhcode_welch(cx_savedata_SOMp5_stat(1),cx_datasaveSOM{1,4},cx_savedata_SOMp5_stat(2),cx_datasaveSOM{1,5},length(cx_savedata_SOMp5),cx_datasaveSOM{1,3})

[h,p]= kstest2(cx_savedata_ADp5,cx_datasaveAD{1,1})
[h,p]= kstest2(cx_savedata_SOMp5,cx_datasaveSOM{1,1})

box off
set(gca,'TickDir','out')







