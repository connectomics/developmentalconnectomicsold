%% FIGURE_03
%{
% load raw data
cx_path = 'Z:\Data\goura\FigUpdates_Revision\RawData_NML\';
[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_09-07-2020.xlsx'));
Age = [7 9 14 28];

%% Fractional innervation of soma targets
%P7 Soma Axons
ToSel_1 = find(cx_data(:,1)== 5& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_5 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;19 for Soma specificity; 25 for syn density; 33 for binarised innervation
SMSyn_5 = cx_data(ToSel_1,6);
SMDens_5 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
SMBulkInn5 = sum(cx_data(ToSel_1,9)-1)/sum(cx_data(ToSel_1,6)-1);
SMBulkDens5 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

ToSel_1 = find(cx_data(:,1)== 7.1& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_7 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;19 for Soma specificity; 25 for syn density; 33 for binarised innervation
SMSyn_7 = cx_data(ToSel_1,6);
SMDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
SMBulkInn7 = sum(cx_data(ToSel_1,9)-1)/sum(cx_data(ToSel_1,6)-1);
SMBulkDens7 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

% %for pooling both P7 datasets
% ToSel_1a = find(cx_data(:,1)== 7.1& cx_data(:,3)== 3); 
% ToSel_1b = find(cx_data(:,1)== 7.2& cx_data(:,3)== 3);%use the age and seeding as filter
% ToSel_1 = cat(1,ToSel_1a,ToSel_1b);
% SMInn_7 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;19 for Soma specificity; 25 for syn density; 33 for binarised innervation
% SMSyn_7 = cx_data(ToSel_1,6);
% SMDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
% SMBulkInn7 = sum(cx_data(ToSel_1,9)-1)/sum(cx_data(ToSel_1,6)-1);
% SMBulkDens7 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));


%P9 Soma Axons
%P9L4n1 dataset
ToSel_2 = find(cx_data(:,1)== 9& cx_data(:,3)== 3);
SMInn_9a = (cx_data(ToSel_2,9)-1)./(cx_data(ToSel_2,6)-1); 
SMSyn_9a = cx_data(ToSel_2,6); 
SMDens_9a = (cx_data(ToSel_2,6))./(cx_data(ToSel_2,4));
%P9L4n2 dataset
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3); 
SMInn_9b = (cx_data(ToSel_3,9)-1)./(cx_data(ToSel_3,6)-1);
SMSyn_9b = cx_data(ToSel_3,6); 
SMDens_9b = (cx_data(ToSel_3,6))./(cx_data(ToSel_3,4));
SMBulkInn9 = (sum((cx_data(ToSel_2,9)-1))+sum((cx_data(ToSel_3,9)-1)))/(sum((cx_data(ToSel_2,6)-1))+sum((cx_data(ToSel_3,6)-1)));
SMBulkDens9 = ((sum(cx_data(ToSel_2,6)))+(sum(cx_data(ToSel_3,6))))/((sum(cx_data(ToSel_2,4)))+(sum(cx_data(ToSel_3,4))));


%P14 Soma Axons
%P14L4n1 dataset
ToSel_4 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 3);
SMInn_14a = (cx_data(ToSel_4,9)-1)./(cx_data(ToSel_4,6)-1);
SMSyn_14a = cx_data(ToSel_4,6); 
SMDens_14a = (cx_data(ToSel_4,6))./(cx_data(ToSel_4,4));
%P14L4n2 dataset
ToSel_5 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_14b = (cx_data(ToSel_5,9)-1)./(cx_data(ToSel_5,6)-1);
SMSyn_14b = cx_data(ToSel_5,6); 
SMDens_14b =(cx_data(ToSel_5,6))./(cx_data(ToSel_5,4));
SMBulkInn14 = (sum((cx_data(ToSel_4,9)-1))+sum((cx_data(ToSel_5,9)-1)))/(sum((cx_data(ToSel_4,6)-1))+sum((cx_data(ToSel_5,6)-1)));
SMBulkDens14 = ((sum(cx_data(ToSel_4,6)))+(sum(cx_data(ToSel_5,6))))/((sum(cx_data(ToSel_4,4)))+(sum(cx_data(ToSel_5,4))));

%P28 Soma Axons
ToSel_6 = find(cx_data(:,1)== 28& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_28 =(cx_data(ToSel_6,9)-1)./(cx_data(ToSel_6,6)-1);
SMSyn_28 = cx_data(ToSel_6,6); 
SMDens_28 = (cx_data(ToSel_6,6))./(cx_data(ToSel_6,4));
SMBulkInn28 = sum(cx_data(ToSel_6,9)-1)/sum(cx_data(ToSel_6,6)-1);
SMBulkDens28 = sum(cx_data(ToSel_6,6))/sum(cx_data(ToSel_6,4));

% Combine two P9 and P14 datasets
SMSyn_9 = cat(1,SMSyn_9a,SMSyn_9b); SMInn_9 = cat(1,SMInn_9a,SMInn_9b); SMDens_9 = cat(1,SMDens_9a,SMDens_9b); 
SMSyn_14 = cat(1,SMSyn_14a,SMSyn_14b); SMInn_14 = cat(1,SMInn_14a,SMInn_14b); SMDens_14 = cat(1,SMDens_14a,SMDens_14b); 

% Concatenate all the parameters and sort
SM7all = cat(2,SMSyn_7,SMInn_7,SMDens_7); 
SM9all = cat(2,SMSyn_9,SMInn_9,SMDens_9);
SM14all = cat(2,SMSyn_14,SMInn_14,SMDens_14);
SM28all = cat(2,SMSyn_28,SMInn_28,SMDens_28);
 
SM7sort = sortrows(SM7all,1);
SM9sort = sortrows(SM9all,1);
SM14sort = sortrows(SM14all,1);
SM28sort = sortrows(SM28all,1);
% THRESHOLDING based on nSyn
%thrA = <10; thrB = 10-20; thrC = > 20syn
SelA = find(SM7sort(:,1) < 10);
SelB = find(SM7sort(:,1) > 9 & SM7sort(:,1) <20);
SelC = find(SM7sort(:,1) > 19); 
SM7_thrA_Inn = SM7sort(SelA,2); SM7_thrB_Inn = SM7sort(SelB,2); SM7_thrC_Inn = SM7sort(SelC,2);
SM7_thrA_Dens = SM7sort(SelA,3); SM7_thrB_Dens = SM7sort(SelB,3); SM7_thrC_Dens = SM7sort(SelC,3);

SelA = find(SM9sort(:,1) < 10);
SelB = find(SM9sort(:,1) > 9 & SM9sort(:,1) <20);
SelC = find(SM9sort(:,1) > 19); 
SM9_thrA_Inn = SM9sort(SelA,2); SM9_thrB_Inn = SM9sort(SelB,2); SM9_thrC_Inn = SM9sort(SelC,2);
SM9_thrA_Dens = SM9sort(SelA,3); SM9_thrB_Dens = SM9sort(SelB,3); SM9_thrC_Dens = SM9sort(SelC,3);

SelA = find(SM14sort(:,1) < 10);
SelB = find(SM14sort(:,1) > 9 & SM14sort(:,1) <20);
SelC = find(SM14sort(:,1) > 19); 
SM14_thrA_Inn = SM14sort(SelA,2); SM14_thrB_Inn = SM14sort(SelB,2); SM14_thrC_Inn = SM14sort(SelC,2);
SM14_thrA_Dens = SM14sort(SelA,3); SM14_thrB_Dens = SM14sort(SelB,3); SM14_thrC_Dens = SM14sort(SelC,3);
  
SelA = find(SM28sort(:,1) < 10);
SelB = find(SM28sort(:,1) > 9 & SM28sort(:,1) <20);
SelC = find(SM28sort(:,1) > 19); 
SM28_thrA_Inn = SM28sort(SelA,2); SM28_thrB_Inn = SM28sort(SelB,2); SM28_thrC_Inn = SM28sort(SelC,2);
SM28_thrA_Dens = SM28sort(SelA,3); SM28_thrB_Dens = SM28sort(SelB,3); SM28_thrC_Dens = SM28sort(SelC,3);

% Bootstrapping to get error means
SMInn_7_bstrp = bootstrp(10,@mean,SMInn_7); SE_SMInn_7 = std(SMInn_7_bstrp);
SMInn_9_bstrp = bootstrp(10,@mean,SMInn_9); SE_SMInn_9 = std(SMInn_9_bstrp); 
SMInn_14_bstrp = bootstrp(10,@mean,SMInn_14); SE_SMInn_14 = std(SMInn_14_bstrp); 
SMInn_28_bstrp = bootstrp(10,@mean,SMInn_28); SE_SMInn_28 = std(SMInn_28_bstrp);
BootStrpData_SMInn = cat(2,SMInn_7_bstrp,SMInn_9_bstrp,SMInn_14_bstrp,SMInn_28_bstrp);
 
SMDens_7_bstrp = bootstrp(10,@mean,SMDens_7); SE_SMDens_7 = std(SMDens_7_bstrp);
SMDens_9_bstrp = bootstrp(10,@mean,SMDens_9); SE_SMDens_9 = std(SMDens_9_bstrp); 
SMDens_14_bstrp = bootstrp(10,@mean,SMDens_14); SE_SMDens_14 = std(SMDens_14_bstrp); 
SMDens_28_bstrp = bootstrp(10,@mean,SMDens_28); SE_SMDens_28 = std(SMDens_28_bstrp);
BootStrpData_SMDens = cat(2,SMDens_7_bstrp,SMDens_9_bstrp,SMDens_14_bstrp,SMDens_28_bstrp);
 % Concatenate lines
SE_Inn = cat(1,SE_SMInn_7,SE_SMInn_9,SE_SMInn_14,SE_SMInn_28);
SE_Dens = cat(1,SE_SMDens_7,SE_SMDens_9,SE_SMDens_14,SE_SMDens_28);
SMBulkInn = cat(1,SMBulkInn7,SMBulkInn9,SMBulkInn14,SMBulkInn28); 
SMBulkDens = cat(1,SMBulkDens7,SMBulkDens9,SMBulkDens14,SMBulkDens28);
SE_Inn_plus = SMBulkInn + SE_Inn;
SE_Inn_minus = SMBulkInn - SE_Inn;
SE_Dens_plus = SMBulkDens + SE_Dens;
SE_Dens_minus = SMBulkDens - SE_Dens;

SEMerrInn = cat(1,std(SMInn_7)/sqrt(numel(SMInn_7)),std(SMInn_9)/sqrt(numel(SMInn_9)),...
std(SMInn_14)/sqrt(numel(SMInn_14)),std(SMInn_28)/sqrt(numel(SMInn_28)));
%err with P5
% SEMerrInn = cat(1,std(SMInn_5)/sqrt(numel(SMInn_5)),std(SMInn_7)/sqrt(numel(SMInn_7)),std(SMInn_9)/sqrt(numel(SMInn_9)),...
% std(SMInn_14)/sqrt(numel(SMInn_14)),std(SMInn_28)/sqrt(numel(SMInn_28)));

 % Concatenate data for box plots
InnData = cat(1,SMInn_7, SMInn_9, SMInn_14, SMInn_28);
DensData = cat(1,SMDens_7, SMDens_9, SMDens_14, SMDens_28);
% Grouping
g1 = ones(size(SMInn_7));
g2 = 2*ones(size(SMInn_9));
g3 = 3*ones(size(SMInn_14));
g4 = 4*ones(size(SMInn_28));
Group = cat(1,g1,g2,g3,g4);

% Plotting
% Innervation fractions
figure;
boxplot(InnData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Fractional innervation of soma targets')
xlabel('Postnatal Age (days)')
plot(Age,SMBulkInn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,SE_Inn_plus,'--k')% lplus err line
plot(Age,SE_Inn_minus,'--k')% minus err line
scatter(ones(numel(SM7_thrA_Inn),1)*7+rand(numel(SM7_thrA_Inn),1)*.75-.45, SM7_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.0005);
scatter(ones(numel(SM7_thrB_Inn),1)*7+rand(numel(SM7_thrB_Inn),1)*.75-.45, SM7_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM7_thrC_Inn),1)*7+rand(numel(SM7_thrC_Inn),1)*.75-.45, SM7_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM9_thrA_Inn),1)*9+rand(numel(SM9_thrA_Inn),1)*.75-.45, SM9_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM9_thrB_Inn),1)*9+rand(numel(SM9_thrB_Inn),1)*.75-.45, SM9_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM9_thrC_Inn),1)*9+rand(numel(SM9_thrC_Inn),1)*.75-.45, SM9_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM14_thrA_Inn),1)*14+rand(numel(SM14_thrA_Inn),1)*.75-.45, SM14_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM14_thrB_Inn),1)*14+rand(numel(SM14_thrB_Inn),1)*.75-.45, SM14_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM14_thrC_Inn),1)*14+rand(numel(SM14_thrC_Inn),1)*.75-.45, SM14_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM28_thrA_Inn),1)*28+rand(numel(SM28_thrA_Inn),1)*.75-.45, SM28_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM28_thrB_Inn),1)*28+rand(numel(SM28_thrB_Inn),1)*.75-.45, SM28_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM28_thrC_Inn),1)*28+rand(numel(SM28_thrC_Inn),1)*.75-.45, SM28_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
title('L4 Innervation fractions - Soma axons')

% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.6])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapse density (syn/um axonal path length)')
xlabel('Postnatal Age (days)')
plot(Age,SMBulkDens,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,SE_Dens_plus,'--k')% lplus err line
plot(Age,SE_Dens_minus,'--k')% minus err line
scatter(ones(numel(SM7_thrA_Dens),1)*7+rand(numel(SM7_thrA_Dens),1)*.75-.45, SM7_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM7_thrB_Dens),1)*7+rand(numel(SM7_thrB_Dens),1)*.75-.45, SM7_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM7_thrC_Dens),1)*7+rand(numel(SM7_thrC_Dens),1)*.75-.45, SM7_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM9_thrA_Dens),1)*9+rand(numel(SM9_thrA_Dens),1)*.75-.45, SM9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM9_thrB_Dens),1)*9+rand(numel(SM9_thrB_Dens),1)*.75-.45, SM9_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM9_thrC_Dens),1)*9+rand(numel(SM9_thrC_Dens),1)*.75-.45, SM9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM14_thrA_Dens),1)*14+rand(numel(SM14_thrA_Dens),1)*.75-.45, SM14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM14_thrB_Dens),1)*14+rand(numel(SM14_thrB_Dens),1)*.75-.45, SM14_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM14_thrC_Dens),1)*14+rand(numel(SM14_thrC_Dens),1)*.75-.45, SM14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM28_thrA_Dens),1)*28+rand(numel(SM28_thrA_Dens),1)*.75-.45, SM28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM28_thrB_Dens),1)*28+rand(numel(SM28_thrB_Dens),1)*.75-.45, SM28_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM28_thrC_Dens),1)*28+rand(numel(SM28_thrC_Dens),1)*.75-.45, SM28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
title('L4 Synapse densities - Soma axons')
SEMerrDens = cat(1,std(SMDens_7)/sqrt(numel(SMDens_7)),std(SMDens_9)/sqrt(numel(SMDens_9)),...
std(SMDens_14)/sqrt(numel(SMDens_14)),std(SMDens_28)/sqrt(numel(SMDens_28)));

%% Fractional innervation and axonal synapse density - AD axons
%P7 AD Axons
%for P5
% ToSel_1 = find(cx_data(:,1)== 5& cx_data(:,3)== 1);
% ADInn_5 = (cx_data(ToSel_1,7)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;
% ADSyn_5 = cx_data(ToSel_1,6);
% ADDens_5 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
% ADBulkInn5 = sum(cx_data(ToSel_1,7)-1)/sum(cx_data(ToSel_1,6)-1);
% ADBulkDens5 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

ToSel_1 = find(cx_data(:,1)== 7.1& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_7 = (cx_data(ToSel_1,7)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;
ADSyn_7 = cx_data(ToSel_1,6);
ADDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
ADBulkInn7 = sum(cx_data(ToSel_1,7)-1)/sum(cx_data(ToSel_1,6)-1);
ADBulkDens7 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

%Pooling both P7 datasets
% ToSel_1a = find(cx_data(:,1)== 7.1& cx_data(:,3)== 1); 
% ToSel_1b = find(cx_data(:,1)== 7.2& cx_data(:,3)== 1);
% ToSel_1 = cat(1,ToSel_1a,ToSel_1b);%use the age and seeding as filter
% ADInn_7 = (cx_data(ToSel_1,7)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;
% ADSyn_7 = cx_data(ToSel_1,6);
% ADDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
% ADBulkInn7 = sum(cx_data(ToSel_1,7)-1)/sum(cx_data(ToSel_1,6)-1);
% ADBulkDens7 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));
  
%P9 AD Axons
%P9L4n1 dataset
ToSel_2 = find(cx_data(:,1)== 9& cx_data(:,3)== 1);
ADInn_9a = (cx_data(ToSel_2,7)-1)./(cx_data(ToSel_2,6)-1); 
ADSyn_9a = cx_data(ToSel_2,6); 
ADDens_9a = (cx_data(ToSel_2,6))./(cx_data(ToSel_2,4));
%P9L4n2 dataset
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 1); 
ADInn_9b = (cx_data(ToSel_3,7)-1)./(cx_data(ToSel_3,6)-1);
ADSyn_9b = cx_data(ToSel_3,6); 
ADDens_9b = (cx_data(ToSel_3,6))./(cx_data(ToSel_3,4));
ADBulkInn9 = (sum((cx_data(ToSel_2,7)-1))+sum((cx_data(ToSel_3,7)-1)))/(sum((cx_data(ToSel_2,6)-1))+sum((cx_data(ToSel_3,6)-1)));
ADBulkDens9 = ((sum(cx_data(ToSel_2,6)))+(sum(cx_data(ToSel_3,6))))/((sum(cx_data(ToSel_2,4)))+(sum(cx_data(ToSel_3,4))));

%P14 AD Axons
%P14L4n1 dataset
ToSel_4 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 1);
ADInn_14a = (cx_data(ToSel_4,7)-1)./(cx_data(ToSel_4,6)-1);
ADSyn_14a = cx_data(ToSel_4,6); 
ADDens_14a = (cx_data(ToSel_4,6))./(cx_data(ToSel_4,4));
%P14L4n2 dataset
ToSel_5 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_14b = (cx_data(ToSel_5,7)-1)./(cx_data(ToSel_5,6)-1);
ADSyn_14b = cx_data(ToSel_5,6); 
ADDens_14b =(cx_data(ToSel_5,6))./(cx_data(ToSel_5,4));
ADBulkInn14 = (sum((cx_data(ToSel_4,7)-1))+sum((cx_data(ToSel_5,7)-1)))/(sum((cx_data(ToSel_4,6)-1))+sum((cx_data(ToSel_5,6)-1)));
ADBulkDens14 = ((sum(cx_data(ToSel_4,6)))+(sum(cx_data(ToSel_5,6))))/((sum(cx_data(ToSel_4,4)))+(sum(cx_data(ToSel_5,4))));
 
%P28 AD Axons
ToSel_6 = find(cx_data(:,1)== 28& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_28 =(cx_data(ToSel_6,7)-1)./(cx_data(ToSel_6,6)-1);
ADSyn_28 = cx_data(ToSel_6,6); 
ADDens_28 = (cx_data(ToSel_6,6))./(cx_data(ToSel_6,4));
ADBulkInn28 = sum(cx_data(ToSel_6,7)-1)/sum(cx_data(ToSel_6,6)-1);
ADBulkDens28 = sum(cx_data(ToSel_6,6))/sum(cx_data(ToSel_6,4));
 
% Combine two P9 and P14 datasets
ADSyn_9 = cat(1,ADSyn_9a,ADSyn_9b); ADInn_9 = cat(1,ADInn_9a,ADInn_9b); ADDens_9 = cat(1,ADDens_9a,ADDens_9b); 
ADSyn_14 = cat(1,ADSyn_14a,ADSyn_14b); ADInn_14 = cat(1,ADInn_14a,ADInn_14b); ADDens_14 = cat(1,ADDens_14a,ADDens_14b); 
 
% Concatenate all the parameters and sort
AD7all = cat(2,ADSyn_7,ADInn_7,ADDens_7); 
AD9all = cat(2,ADSyn_9,ADInn_9,ADDens_9);
AD14all = cat(2,ADSyn_14,ADInn_14,ADDens_14);
AD28all = cat(2,ADSyn_28,ADInn_28,ADDens_28);
 
AD7sort = sortrows(AD7all,1);
AD9sort = sortrows(AD9all,1);
AD14sort = sortrows(AD14all,1);
AD28sort = sortrows(AD28all,1);
% THRESHOLDING based on nSyn
%thrA = <10; thrB = 10-20; thrC = > 20syn
SelA = find(AD7sort(:,1) < 10);
SelB = find(AD7sort(:,1) > 9 & AD7sort(:,1) <20);
SelC = find(AD7sort(:,1) > 19); 
AD7_thrA_Inn = AD7sort(SelA,2); AD7_thrB_Inn = AD7sort(SelB,2); AD7_thrC_Inn = AD7sort(SelC,2);
AD7_thrA_Dens = AD7sort(SelA,3); AD7_thrB_Dens = AD7sort(SelB,3); AD7_thrC_Dens = AD7sort(SelC,3);
 
SelA = find(AD9sort(:,1) < 10);
SelB = find(AD9sort(:,1) > 9 & AD9sort(:,1) <20);
SelC = find(AD9sort(:,1) > 19); 
AD9_thrA_Inn = AD9sort(SelA,2); AD9_thrB_Inn = AD9sort(SelB,2); AD9_thrC_Inn = AD9sort(SelC,2);
AD9_thrA_Dens = AD9sort(SelA,3); AD9_thrB_Dens = AD9sort(SelB,3); AD9_thrC_Dens = AD9sort(SelC,3);
 
SelA = find(AD14sort(:,1) < 10);
SelB = find(AD14sort(:,1) > 9 & AD14sort(:,1) <20);
SelC = find(AD14sort(:,1) > 19); 
AD14_thrA_Inn = AD14sort(SelA,2); AD14_thrB_Inn = AD14sort(SelB,2); AD14_thrC_Inn = AD14sort(SelC,2);
AD14_thrA_Dens = AD14sort(SelA,3); AD14_thrB_Dens = AD14sort(SelB,3); AD14_thrC_Dens = AD14sort(SelC,3);
  
SelA = find(AD28sort(:,1) < 10);
SelB = find(AD28sort(:,1) > 9 & AD28sort(:,1) <20);
SelC = find(AD28sort(:,1) > 19); 
AD28_thrA_Inn = AD28sort(SelA,2); AD28_thrB_Inn = AD28sort(SelB,2); AD28_thrC_Inn = AD28sort(SelC,2);
AD28_thrA_Dens = AD28sort(SelA,3); AD28_thrB_Dens = AD28sort(SelB,3); AD28_thrC_Dens = AD28sort(SelC,3);
 
% Bootstrapping to get error means
ADInn_7_bstrp = bootstrp(10,@mean,ADInn_7); SE_ADInn_7 = std(ADInn_7_bstrp);
ADInn_9_bstrp = bootstrp(10,@mean,ADInn_9); SE_ADInn_9 = std(ADInn_9_bstrp); 
ADInn_14_bstrp = bootstrp(10,@mean,ADInn_14); SE_ADInn_14 = std(ADInn_14_bstrp); 
ADInn_28_bstrp = bootstrp(10,@mean,ADInn_28); SE_ADInn_28 = std(ADInn_28_bstrp);
BootStrpData_ADInn = cat(2,ADInn_7_bstrp,ADInn_9_bstrp,ADInn_14_bstrp,ADInn_28_bstrp);
 
ADDens_7_bstrp = bootstrp(10,@mean,ADDens_7); SE_ADDens_7 = std(ADDens_7_bstrp);
ADDens_9_bstrp = bootstrp(10,@mean,ADDens_9); SE_ADDens_9 = std(ADDens_9_bstrp); 
ADDens_14_bstrp = bootstrp(10,@mean,ADDens_14); SE_ADDens_14 = std(ADDens_14_bstrp); 
ADDens_28_bstrp = bootstrp(10,@mean,ADDens_28); SE_ADDens_28 = std(ADDens_28_bstrp);
BootStrpData_ADDens = cat(2,ADDens_7_bstrp,ADDens_9_bstrp,ADDens_14_bstrp,ADDens_28_bstrp);
% Concatenate lines
SE_Inn = cat(1,SE_ADInn_7,SE_ADInn_9,SE_ADInn_14,SE_ADInn_28);
SE_Dens = cat(1,SE_ADDens_7,SE_ADDens_9,SE_ADDens_14,SE_ADDens_28);
ADBulkInn = cat(1,ADBulkInn7,ADBulkInn9,ADBulkInn14,ADBulkInn28); 
ADBulkDens = cat(1,ADBulkDens7,ADBulkDens9,ADBulkDens14,ADBulkDens28);
SE_Inn_plus = ADBulkInn + SE_Inn;
SE_Inn_minus = ADBulkInn - SE_Inn;
SE_Dens_plus = ADBulkDens + SE_Dens;
SE_Dens_minus = ADBulkDens - SE_Dens;

SEMerrInn = cat(1,std(ADInn_7)/sqrt(numel(ADInn_7)),std(ADInn_9)/sqrt(numel(ADInn_9)),...
std(ADInn_14)/sqrt(numel(ADInn_14)),std(ADInn_28)/sqrt(numel(ADInn_28)));

SEMerrDens = cat(1,std(ADDens_7)/sqrt(numel(ADDens_7)),std(ADDens_9)/sqrt(numel(ADDens_9)),...
std(ADDens_14)/sqrt(numel(ADDens_14)),std(ADDens_28)/sqrt(numel(ADDens_28)));


 % Concatenate data for box plots
InnData = cat(1,ADInn_7, ADInn_9, ADInn_14, ADInn_28);
DensData = cat(1,ADDens_7, ADDens_9, ADDens_14, ADDens_28);
% Grouping
g1 = ones(size(ADInn_7));
g2 = 2*ones(size(ADInn_9));
g3 = 3*ones(size(ADInn_14));
g4 = 4*ones(size(ADInn_28));
Group = cat(1,g1,g2,g3,g4);
 
% Plotting
% Innervation fractions
figure;
boxplot(InnData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Fractional innervation of AD targets')
xlabel('Postnatal Age (days)')
plot(Age,ADBulkInn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,SE_Inn_plus,'--k')% lplus err line
plot(Age,SE_Inn_minus,'--k')% minus err line
scatter(ones(numel(AD7_thrA_Inn),1)*7+rand(numel(AD7_thrA_Inn),1)*.75-.45, AD7_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.0005);
scatter(ones(numel(AD7_thrB_Inn),1)*7+rand(numel(AD7_thrB_Inn),1)*.75-.45, AD7_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD7_thrC_Inn),1)*7+rand(numel(AD7_thrC_Inn),1)*.75-.45, AD7_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD9_thrA_Inn),1)*9+rand(numel(AD9_thrA_Inn),1)*.75-.45, AD9_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD9_thrB_Inn),1)*9+rand(numel(AD9_thrB_Inn),1)*.75-.45, AD9_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD9_thrC_Inn),1)*9+rand(numel(AD9_thrC_Inn),1)*.75-.45, AD9_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD14_thrA_Inn),1)*14+rand(numel(AD14_thrA_Inn),1)*.75-.45, AD14_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD14_thrB_Inn),1)*14+rand(numel(AD14_thrB_Inn),1)*.75-.45, AD14_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD14_thrC_Inn),1)*14+rand(numel(AD14_thrC_Inn),1)*.75-.45, AD14_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD28_thrA_Inn),1)*28+rand(numel(AD28_thrA_Inn),1)*.75-.45, AD28_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD28_thrB_Inn),1)*28+rand(numel(AD28_thrB_Inn),1)*.75-.45, AD28_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD28_thrC_Inn),1)*28+rand(numel(AD28_thrC_Inn),1)*.75-.45, AD28_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
title('L4 Innervation fractions - AD axons')
 
% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.6])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapse density (syn/um axonal path length)')
xlabel('Postnatal Age (days)')
plot(Age,ADBulkDens,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,SE_Dens_plus,'--k')% lplus err line
plot(Age,SE_Dens_minus,'--k')% minus err line
scatter(ones(numel(AD7_thrA_Dens),1)*7+rand(numel(AD7_thrA_Dens),1)*.75-.45, AD7_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD7_thrB_Dens),1)*7+rand(numel(AD7_thrB_Dens),1)*.75-.45, AD7_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD7_thrC_Dens),1)*7+rand(numel(AD7_thrC_Dens),1)*.75-.45, AD7_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD9_thrA_Dens),1)*9+rand(numel(AD9_thrA_Dens),1)*.75-.45, AD9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD9_thrB_Dens),1)*9+rand(numel(AD9_thrB_Dens),1)*.75-.45, AD9_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD9_thrC_Dens),1)*9+rand(numel(AD9_thrC_Dens),1)*.75-.45, AD9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD14_thrA_Dens),1)*14+rand(numel(AD14_thrA_Dens),1)*.75-.45, AD14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD14_thrB_Dens),1)*14+rand(numel(AD14_thrB_Dens),1)*.75-.45, AD14_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD14_thrC_Dens),1)*14+rand(numel(AD14_thrC_Dens),1)*.75-.45, AD14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD28_thrA_Dens),1)*28+rand(numel(AD28_thrA_Dens),1)*.75-.45, AD28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD28_thrB_Dens),1)*28+rand(numel(AD28_thrB_Dens),1)*.75-.45, AD28_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD28_thrC_Dens),1)*28+rand(numel(AD28_thrC_Dens),1)*.75-.45, AD28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
title('L4 Synapse densities - AD axons')
%}

%% AVERAGE SYNAPSES PER TARGET - SOMA Axons
clear all
% cx_path = 'Z:\Data\goura\FigureUpdates\RawData_Excels\';
[cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_16-07-2020.xlsx','Sheet3');
Age = [7 9 14 28];
%P7 Soma Axons
ToSel_1 = find(cx_data(:,1)==7& cx_data(:,3)== 3);
SM_AvgSyn_7 = cx_data(ToSel_1,5)./cx_data(ToSel_1,6);
%P9 Soma Axons
ToSel_2a = find(cx_data(:,1)==9.1& cx_data(:,3)== 3);
ToSel_2b = find(cx_data(:,1)==9.2& cx_data(:,3)== 3);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
SM_AvgSyn_9 = cx_data(ToSel_2,5)./cx_data(ToSel_2,6);
%P14 Soma Axons
ToSel_3a = find(cx_data(:,1)==14.1& cx_data(:,3)== 3);
ToSel_3b = find(cx_data(:,1)==14.2& cx_data(:,3)== 3);
ToSel_3 = cat(1,ToSel_3a,ToSel_3b);
SM_AvgSyn_14 = cx_data(ToSel_3,5)./cx_data(ToSel_3,6);
%P28 Soma Axons
ToSel_4 = find(cx_data(:,1)==28& cx_data(:,3)== 3);
SM_AvgSyn_28 = cx_data(ToSel_4,5)./cx_data(ToSel_4,6);

SM_MeanAvgSyn = cat(1,mean(SM_AvgSyn_7),mean(SM_AvgSyn_9),mean(SM_AvgSyn_14),mean(SM_AvgSyn_28));
Err7 = std(SM_AvgSyn_7)/sqrt(numel(SM_AvgSyn_7));
Err9 = std(SM_AvgSyn_9)/sqrt(numel(SM_AvgSyn_9));
Err14 = std(SM_AvgSyn_14)/sqrt(numel(SM_AvgSyn_14));
Err28 = std(SM_AvgSyn_28)/sqrt(numel(SM_AvgSyn_28));
SM_AvgSyn_Err = cat(1,Err7,Err9,Err14,Err28);

Data_SM_AvgSyn = cat(1,SM_AvgSyn_7,SM_AvgSyn_9,SM_AvgSyn_14,SM_AvgSyn_28);
g1 = ones(size(SM_AvgSyn_7));
g2 = 2*ones(size(SM_AvgSyn_9));
g3 = 3*ones(size(SM_AvgSyn_14));
g4 = 4*ones(size(SM_AvgSyn_28));
Group = cat(1,g1,g2,g3,g4);
%Plotting
figure
boxplot(Data_SM_AvgSyn,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Average number of synapses per soma')
xlabel('Postnatal Age (days)')
plot(Age,SM_MeanAvgSyn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(SM_MeanAvgSyn+SM_AvgSyn_Err),'--k')% lplus err line
plot(Age,(SM_MeanAvgSyn-SM_AvgSyn_Err),'--k')% minus err line
scatter(ones(numel(SM_AvgSyn_7),1)*7+rand(numel(SM_AvgSyn_7),1)*.75-.45, SM_AvgSyn_7,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(SM_AvgSyn_9),1)*9+rand(numel(SM_AvgSyn_9),1)*.75-.45, SM_AvgSyn_9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(SM_AvgSyn_14),1)*14+rand(numel(SM_AvgSyn_14),1)*.75-.45, SM_AvgSyn_14,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(SM_AvgSyn_28),1)*28+rand(numel(SM_AvgSyn_28),1)*.75-.45, SM_AvgSyn_28,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
title('L4 Soma Axons - Average nSyn per target')

% AD axons

%P7 AD Axons
ToSel_1 = find(cx_data(:,1)==7& cx_data(:,3)== 1);
AD_AvgSyn_7 = cx_data(ToSel_1,5)./cx_data(ToSel_1,6);
%P9 Soma Axons
ToSel_2a = find(cx_data(:,1)==9.1& cx_data(:,3)== 1);
ToSel_2b = find(cx_data(:,1)==9.2& cx_data(:,3)== 3);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
AD_AvgSyn_9 = cx_data(ToSel_2,5)./cx_data(ToSel_2,6);
%P14 Soma Axons
ToSel_3a = find(cx_data(:,1)==14.1& cx_data(:,3)== 1);
ToSel_3b = find(cx_data(:,1)==14.2& cx_data(:,3)== 1);
ToSel_3 = cat(1,ToSel_3a,ToSel_3b);
AD_AvgSyn_14 = cx_data(ToSel_3,5)./cx_data(ToSel_3,6);
%P28 Soma Axons
ToSel_4 = find(cx_data(:,1)==28& cx_data(:,3)== 1);
AD_AvgSyn_28 = cx_data(ToSel_4,5)./cx_data(ToSel_4,6);
 
AD_MeanAvgSyn = cat(1,mean(AD_AvgSyn_7),mean(AD_AvgSyn_9),mean(AD_AvgSyn_14),mean(AD_AvgSyn_28));
Err7 = std(AD_AvgSyn_7)/sqrt(numel(AD_AvgSyn_7));
Err9 = std(AD_AvgSyn_9)/sqrt(numel(AD_AvgSyn_9));
Err14 = std(AD_AvgSyn_14)/sqrt(numel(AD_AvgSyn_14));
Err28 = std(AD_AvgSyn_28)/sqrt(numel(AD_AvgSyn_28));
AD_AvgSyn_Err = cat(1,Err7,Err9,Err14,Err28);
 
Data_AD_AvgSyn = cat(1,AD_AvgSyn_7,AD_AvgSyn_9,AD_AvgSyn_14,AD_AvgSyn_28);
g1 = ones(size(AD_AvgSyn_7));
g2 = 2*ones(size(AD_AvgSyn_9));
g3 = 3*ones(size(AD_AvgSyn_14));
g4 = 4*ones(size(AD_AvgSyn_28));
Group = cat(1,g1,g2,g3,g4);
%Plotting
figure
boxplot(Data_AD_AvgSyn,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Average number of synapses per AD')
xlabel('Postnatal Age (days)')
plot(Age,AD_MeanAvgSyn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(AD_MeanAvgSyn+AD_AvgSyn_Err),'--k')% lplus err line
plot(Age,(AD_MeanAvgSyn-AD_AvgSyn_Err),'--k')% minus err line
scatter(ones(numel(AD_AvgSyn_7),1)*7+rand(numel(AD_AvgSyn_7),1)*.75-.45, AD_AvgSyn_7,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(AD_AvgSyn_9),1)*9+rand(numel(AD_AvgSyn_9),1)*.75-.45, AD_AvgSyn_9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(AD_AvgSyn_14),1)*14+rand(numel(AD_AvgSyn_14),1)*.75-.45, AD_AvgSyn_14,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(AD_AvgSyn_28),1)*28+rand(numel(AD_AvgSyn_28),1)*.75-.45, AD_AvgSyn_28,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
title('L4 AD Axons - Average nSyn per target')

%% MAPPING OF SOMATIC INPUTS AND CORRESPONDING SOMATIC DIAMETERS
clear all
% cx_path = 'Z:\Data\goura\FigureUpdates\RawData_Excels\';
[cx_data, cx_data_txt] = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L4_RawData_16-07-2020.xlsx','Sheet4');
Age = [7 9 14 28];

%Equivalent Soma diameters
ToSel_1 = find(cx_data(:,1)==7); EqDia7 = cx_data(ToSel_1,5);
ToSel_2 = find(cx_data(:,1)==9); EqDia9 = cx_data(ToSel_2,5);
ToSel_3 = find(cx_data(:,1)==14); EqDia14 = cx_data(ToSel_3,5);
ToSel_4 = find(cx_data(:,1)==28); EqDia28 = cx_data(ToSel_4,5);
MeanEqDia = cat(1,mean(EqDia7),mean(EqDia9),mean(EqDia14),mean(EqDia28));
ErrEqDia = cat(1,(std(EqDia7)/sqrt(numel(EqDia7))),(std(EqDia9)/sqrt(numel(EqDia9))),...
                 (std(EqDia14)/sqrt(numel(EqDia14))),(std(EqDia28)/sqrt(numel(EqDia28))));
%All input synapses
ToSel_5 = find(cx_data(:,7)==7); Syn7 = cx_data(ToSel_5,11);
ToSel_6 = find(cx_data(:,7)==9); Syn9 = cx_data(ToSel_6,11);
ToSel_7 = find(cx_data(:,7)==14); Syn14 = cx_data(ToSel_7,11);
ToSel_8 = find(cx_data(:,7)==28); Syn28 = cx_data(ToSel_8,11);
MeanSyn = cat(1,mean(Syn7),mean(Syn9),mean(Syn14),mean(Syn28));
ErrSyn = cat(1,(std(Syn7)/sqrt(numel(Syn7))),(std(Syn9)/sqrt(numel(Syn9))),...
                 (std(Syn14)/sqrt(numel(Syn14))),(std(Syn28)/sqrt(numel(Syn28))));

g1D = ones(size(EqDia7)); g2D = 2*ones(size(EqDia9)); g3D = 3*ones(size(EqDia14));  g4D = 4*ones(size(EqDia28));
g1S = ones(size(Syn7)); g2S = 2*ones(size(Syn9)); g3S = 3*ones(size(Syn14));  g4S = 4*ones(size(Syn28));
Gr_Dia = cat(1,g1D,g2D,g3D,g4D);
Gr_Syn = cat(1,g1S,g2S,g3S,g4S);
EqDia = cat(1,EqDia7,EqDia9,EqDia14,EqDia28);
SynData = cat(1,Syn7,Syn9,Syn14,Syn28);

%Plotting
figure
yyaxis left
hold on
boxplot(SynData,Gr_Syn,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 100])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapses per soma')
xlabel('Postnatal Age (days)')
plot(Age,MeanSyn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanSyn+ErrSyn),'--m')% lplus err line
plot(Age,(MeanSyn-ErrSyn),'--m')% minus err line
scatter(ones(numel(Syn7),1)*7+rand(numel(Syn7),1)*.75-.45, Syn7,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(Syn9),1)*9+rand(numel(Syn9),1)*.75-.45, Syn9,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(Syn14),1)*14+rand(numel(Syn14),1)*.75-.45, Syn14,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(Syn28),1)*28+rand(numel(Syn28),1)*.75-.45, Syn28,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
yyaxis right
boxplot(EqDia,Gr_Dia,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 25])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Somatic diameter (um)')
xlabel('Postnatal Age (days)')
plot(Age,MeanEqDia,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanEqDia+ErrEqDia),'--k')% lplus err line
plot(Age,(MeanEqDia-ErrEqDia),'--k')% minus err line
scatter(ones(numel(EqDia7),1)*7+rand(numel(EqDia7),1)*.75-.45, EqDia7,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(EqDia9),1)*9+rand(numel(EqDia9),1)*.75-.45, EqDia9,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(EqDia14),1)*14+rand(numel(EqDia14),1)*.75-.45, EqDia14,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(EqDia28),1)*28+rand(numel(EqDia28),1)*.75-.45, EqDia28,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
title('L4 - Somatic input mapping and diameters')