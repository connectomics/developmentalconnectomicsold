%% Gour et al. Axo-axonic analyses redone revision
% all other topics: mhocde_devCon_v4.m

%% Load in final P28 wK data
% from wklink AG July 27
%https://webknossos.brain.mpg.de/annotations/Explorational/5f1ef8c90100000e00a84f90#6707,4601,2166,0,4.206,81812
% copied to mh account, downloaded as nml: 2012-11-23_ex144_st08x2__explorational__mhelmstaedter__a84fc8.nml
% renamed to L23_P28_annotationsAxAx.nml

 cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'
%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

%cx_skelobj = skeleton(fullfile(cx_path,'2012-11-23_ex144_st08x2__explorational__mhelmstaedter__a84fc8_v4.nml'));

% cx_skelLoad = slurpNml(fullfile(cx_path,'2012-11-23_ex144_st08x2__explorational__mhelmstaedter__a84fc8.nml'));

%save(fullfile(cx_path,'L23_P28_annotationsAxAx.mat'),'cx_skelobj');

load(fullfile(cx_path,'L23_P28_annotationsAxAx.mat'),'cx_skelobj');

cx_AISids = find(cx_skelobj.groupId==1)
cx_axonsCompl = find(cx_skelobj.groupId==2)
cx_axonsJustMapping = find(cx_skelobj.groupId==3)

cx_axilim = [-0.05 1.05]

%% Analysis of AIS seeded axons, definition of Axax vs other P28
 cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'
%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

cx_allSkelData = {};
cx_allSkelComments = {};
load(fullfile(cx_path,'L23_P28_annotationsAxAx.mat'),'cx_skelobj');

cx_allSkelData{2} = cx_skelobj;
[comments, treeIdx, nodeIdx] = cx_skelobj.getAllComments;
cx_allSkelComments{2} = {comments,treeIdx,nodeIdx};


i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);    % 1: AIS, 2: partial axons for mapping; 3: complete axons
cx_axonSynData = [];
for jj=1:length(cx_axonIds)
    cx_theseCommentLocs = find(cx_allSkelComments{i}{2}==cx_axonIds(jj));
    cx_theseComments = cx_allSkelComments{i}{1}(cx_theseCommentLocs);
    cx_theseCommentCoords = cx_allSkelData{i}.nodes{cx_axonIds(jj)}(cx_allSkelComments{i}{3}(cx_theseCommentLocs),1:3).*...
        repmat(cx_allSkelData{2}.scale,length(cx_theseCommentLocs),1);
    cx_commentTypes=[];cx_synGrade=repmat(0,length(cx_theseCommentLocs),1);cx_AISnum=cx_synGrade*nan;cx_seedLabel=cx_AISnum;
    cx_AIS_nonSomFlag = cx_AISnum;
    for kk=1:length(cx_theseCommentLocs)
        if strfind(cx_theseComments{kk},'syn')
            if strfind(cx_theseComments{kk},'IS')
                cx_commentTypes(kk)=1;
                cx_IDstr= regexp(cx_theseComments{kk},'AIS[\w\-\_]*','match');
                cx_IDstr = regexp(cx_IDstr,'[0-9]+','match');
                if ~isempty(cx_IDstr)
                    if ~isempty(cx_IDstr{1})
                        cx_AISnum(kk) = str2num(cx_IDstr{1}{1});
                    end
                end
                if strfind(cx_theseComments{kk},'NonSom')
                    cx_AIS_nonSomFlag(kk)=1;
                else
                    cx_AIS_nonSomFlag(kk)=0;
                end
            elseif strfind(cx_theseComments{kk},'shaft')
                cx_commentTypes(kk)=3;
            elseif strfind(cx_theseComments{kk},'spin')
                cx_commentTypes(kk)=4;
            else
                cx_commentTypes(kk)=2;
            end
            if strfind(cx_theseComments{kk},'CL1')
                cx_synGrade(kk)=1;
            elseif strfind(cx_theseComments{kk},'CL2')
                cx_synGrade(kk)=2;
            end
            if strfind(cx_theseComments{kk},'seed')
                cx_seedLabel(kk) = 1;
            end
        else
            cx_commentTypes(kk)=0;
        end
    end
    cx_axonSynData(jj,1) = length(find(cx_commentTypes>0)); % n syn
    cx_axonSynData(jj,2) = length(find(cx_commentTypes==1)); % n AIS syn
    cx_axonSynData(jj,3) = cx_allSkelData{i}.pathLength(cx_axonIds(jj));
    if cx_axonSynData(jj,2)/cx_axonSynData(jj,1)>0.3    % error checking: all non-AIS synapses of likely axax axons require synapse grading
        if length(find(cx_synGrade>0))<cx_axonSynData(jj,1)-cx_axonSynData(jj,2)
            sprintf('problem %d %d',i,jj)
        end
    end
    cx_axonSynData(jj,4) = length(find(cx_synGrade(cx_commentTypes~=2)==1));
    cx_axonSynData(jj,5) = length(find(cx_synGrade(cx_commentTypes~=2)==2));
    
    cx_allSynData{i}{jj} = {cx_theseComments,cx_commentTypes,cx_theseCommentCoords,...
        cx_synGrade,cx_AISnum,cx_seedLabel,cx_AIS_nonSomFlag};
    
end

% AIS ID extraction
cx_AIS_ids = find(cx_allSkelData{i}.groupId==1);    % 1: AIS, 2: partial axons for mapping; 3: complete axons
cx_SkelID_to_AISid=[];
for jj=1:length(cx_AIS_ids)
    cx_IDstr= regexp(cx_allSkelData{i}.names{cx_AIS_ids(jj)},'AIS[\w\-\_]*','match');
    cx_IDstr = regexp(cx_IDstr,'[0-9]+','match');
    if ~isempty(cx_IDstr)
        if ~isempty(cx_IDstr{1})
            cx_IDnum = str2num(cx_IDstr{1}{1});
        end
    end
    
    cx_SkelID_to_AISid(jj,1) = cx_AIS_ids(jj);
    cx_SkelID_to_AISid(jj,2) = cx_IDnum;    
    if strfind(cx_allSkelData{i}.names{cx_AIS_ids(jj)},'NonSom')
        cx_SkelID_to_AISid(jj,3)=1;
    else
        cx_SkelID_to_AISid(jj,3)=0;
    end
end

cx_nSynSel = cx_axonSynData(:,1)>=7;
figure
subplot(1,2,1)
plot(cx_axonSynData(cx_nSynSel,1)./cx_axonSynData(cx_nSynSel,3),(cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),'x');hold on
%plot(cx_axonSynData(~cx_nSynSel,1)./cx_axonSynData(~cx_nSynSel,3),(cx_axonSynData(~cx_nSynSel,2)-1)./(cx_axonSynData(~cx_nSynSel,1)-1),'o');hold on

cx_plot=[];
cx_nSynSel_Ids = find(cx_nSynSel);
for kk=1:length(cx_nSynSel_Ids)
   jj=cx_nSynSel_Ids(kk);
   cx_plot(1) =  (cx_axonSynData(jj,2)-1)./(cx_axonSynData(jj,1)-1);
   cx_plot(2) = (cx_axonSynData(jj,2)-1)./(cx_axonSynData(jj,1)-1-cx_axonSynData(jj,5));    % without class 2 synapses
   plot([1 1]*cx_axonSynData(jj,1)./cx_axonSynData(jj,3),cx_plot,'x-k','MarkerSize',15);
end
title('P28, at least 7 syn');
box off; set(gca,'TickDir','out');
set(gca,'XScal','log');set(gca,'YLim',cx_axilim)

subplot(1,2,2)
%histogram((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
hist((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
view([90 90])
set(gca,'XDir','reverse');set(gca,'XLim',cx_axilim);
box off; set(gca,'TickDir','out');

cx_clustData = [cx_axonSynData(cx_nSynSel_Ids,1)./cx_axonSynData(cx_nSynSel_Ids,3),(cx_axonSynData(cx_nSynSel_Ids,2)-1)./(cx_axonSynData(cx_nSynSel_Ids,1)-1)];
cx_clustAssgn = clusterdata(cx_clustData,'distance','Euclidean','linkage','ward','maxclust',2);
figure
mplot(cx_clustData,'x'); hold on
set(gca,'YLim',cx_axilim);set(gca,'XScal','log');
mplot(cx_clustData(cx_clustAssgn==1,:),'xr'); hold on


% p(AIS|AIS) min max P28
cx_sumdata(1)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1));
cx_fractions1P28=(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1));
cx_sumdata(2)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5));    % without class 2 synapses
cx_fractions1P28_cl1 = (cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5));
cx_sumdata(3)= std(cx_fractions1P28)/sqrt(length(cx_fractions1P28));
cx_sumdata(4)= std(cx_fractions1P28_cl1)/sqrt(length(cx_fractions1P28_cl1));


cx_sumdata(5)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./sum((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1));
cx_fractions2P28=(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1));
cx_sumdata(6)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),5));    % without class 2 synapses
cx_fractions2P28_cl1 = (cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),5));
cx_sumdata(7)= std(cx_fractions2P28)/sqrt(length(cx_fractions2P28));
cx_sumdata(8)= std(cx_fractions2P28_cl1)/sqrt(length(cx_fractions2P28_cl1));

title(sprintf('AxAx %.2f+-%.2f, n=%d; no CL2: %.2f+-%.2f \nNon-axax %.2f+-%.2f, n=%d no CL2: %.2f+-%.2f',cx_sumdata(1)*100,cx_sumdata(3)*100,length(cx_fractions1P28),...
    cx_sumdata(2)*100,cx_sumdata(4)*100,...
    cx_sumdata(5)*100,cx_sumdata(7)*100,length(cx_fractions2P28),...
    cx_sumdata(6)*100,cx_sumdata(8)*100));
    

cx_syndens1P28 = cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)./cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3)*1000;
cx_sumdataDens(1) = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1))/sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3))*1000;
cx_sumdataDens(2) = std(cx_syndens1P28)/sqrt(length(cx_syndens1P28))
cx_sumdataDens(3) = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1))/sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),3))*1000;
cx_syndens2P28 = cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)./cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),3)*1000;
cx_sumdataDens(4) = std(cx_syndens2P28)/sqrt(length(cx_syndens2P28))
cx_totalL1 = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3))/1000/1000;
cx_totalL2 = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),3))/1000/1000;
figure
subplot(2,1,1),histogram(cx_syndens1P28);hold on;set(gca,'XLim',[0 0.5]);
title(sprintf('P14 syn dens AxAx %.3f +-%.3f per µm, L_{ax} %.2fmm; n=%d',cx_sumdataDens(1:2),cx_totalL1,length(cx_syndens1P28)));
subplot(2,1,2),histogram(cx_syndens2P28);hold on;set(gca,'XLim',[0 0.5]);
title(sprintf('Non-AxAx %.3f +-%.3f per µm, L_{ax} %.2fmm; n=%d',...
    cx_sumdataDens(3:4),cx_totalL2,length(cx_syndens2P28)));


%% axon plotting with AIS target IDs, based on previous section P28
cx_savepath = fullfile(cx_path,'AxAxGallery');
cx_showAISrecs=1;
i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);
%find(cx_axonIds==96)    % for display. jj=28

for jj=28%1:length(cx_axonIds)
    figure
    cx_allSkelData{i}.plot(cx_axonIds(jj));hold on
    daspect([1 1 1]);
%     mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}==1,:),'mo');
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}==1,:),'m.'); set(aa,'MarkerSize',20);
    mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}>1,:),'ko');   
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{4}==1,:),'k.');   set(aa,'MarkerSize',20);
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{4}==2,:),'k.');   set(aa,'MarkerSize',10);
    cx_seedComments = find(cx_allSynData{i}{jj}{6}==1);
    for jjj=1:length(cx_seedComments)
        text(cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),1)-1000,cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),2),cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),3),'S');
    end
    cx_AISids = find(~isnan(cx_allSynData{i}{jj}{5}));
    for jjj=1:length(cx_AISids)
        if cx_allSynData{i}{jj}{7}(cx_AISids(jjj))==1   %non-Soma AIS ID
            text(cx_allSynData{i}{jj}{3}(cx_AISids(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_AISids(jjj),2),cx_allSynData{i}{jj}{3}(cx_AISids(jjj),3),sprintf('n%d',cx_allSynData{i}{jj}{5}(cx_AISids(jjj))),'FontSize',6);
        else
            text(cx_allSynData{i}{jj}{3}(cx_AISids(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_AISids(jjj),2),cx_allSynData{i}{jj}{3}(cx_AISids(jjj),3),sprintf('%d',cx_allSynData{i}{jj}{5}(cx_AISids(jjj))),'FontSize',6);
        end
    end
%     cx_class = find(cx_allSynData{i}{jj}{4}>0);
%     for jjj=1:length(cx_class)
%         text(cx_allSynData{i}{jj}{3}(cx_class(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_class(jjj),2),cx_allSynData{i}{jj}{3}(cx_class(jjj),3),sprintf('cl%d',cx_allSynData{i}{jj}{4}(cx_class(jjj))));        
%     end
%     
    set(gca,'XLim',[0 12]*10^4,'YLim',[0 12]*10^4);
    if i==1
        view([90 90]);        set(gca,'XDir','reverse');        xlabel('<WM Pia>')
    elseif i==2
        ylabel('<WM Pia>')
    end
    if cx_showAISrecs
        cx_thisAxonTargetAIS = [cx_allSynData{i}{jj}{5},cx_allSynData{i}{jj}{7}];
        cx_thisAxonTargetAIS = unique(cx_thisAxonTargetAIS(~isnan(cx_thisAxonTargetAIS(:,1)),:),'rows');
        for kkk=1:size(cx_thisAxonTargetAIS,1)
            cx_found = find((cx_SkelID_to_AISid(:,2)==cx_thisAxonTargetAIS(kkk,1))&(cx_SkelID_to_AISid(:,3)==cx_thisAxonTargetAIS(kkk,2)));
            cx_thisCol = 0.4+rand*0.2;
            cx_thisSkelID = cx_SkelID_to_AISid(cx_found,1);
            aa=cx_allSkelData{i}.plot(cx_thisSkelID,cx_thisCol*[1 1 1]);
            cc=fieldnames(aa);
            bb=getfield(aa,cc{1});
            set(bb,'LineWidth',2);                
        end        
    end
    title(sprintf('axon %d, P28, %s',cx_axonIds(jj),cx_allSkelData{i}.names{cx_axonIds(jj)}));
%     saveas(gcf,fullfile(cx_savepath,sprintf('P28_axon%d.png',cx_axonIds(jj))));
%     close(gcf);
end
    

%% AIS input mapping, based on the previous section P28
% completely mapped according to AG (email July 29): AIS001, 002, 003, 004,
% 018
% corresponding to rows 56,57,58,59,48 in cx_AISid_to_SkelID

cx_AISid_to_SkelID = full(sparse(cx_SkelID_to_AISid(:,2),cx_SkelID_to_AISid(:,3)+1,cx_SkelID_to_AISid(:,1)));
% assemble ax-to-AIS connectome
i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);
cx_AISconnectome = sparse(length(cx_axonIds),size(cx_SkelID_to_AISid,1));
for jj=1:length(cx_axonIds)
    cx_theseAISIDs = [cx_allSynData{i}{jj}{5},cx_allSynData{i}{jj}{7}];
    cx_theseAISIDs = cx_theseAISIDs(~isnan(cx_theseAISIDs(:,1)),:);
    cx_theseSkelIDs = cx_AISid_to_SkelID(sub2ind(size(cx_AISid_to_SkelID),cx_theseAISIDs(:,1),cx_theseAISIDs(:,2)+1));
    
    cx_thisCxPiece = sparse(jj,cx_theseSkelIDs,1,length(cx_axonIds),size(cx_SkelID_to_AISid,1));
    cx_AISconnectome = cx_AISconnectome + cx_thisCxPiece;    
end
figure
%spy(cx_AISconnectome);
xlabel('AIS');ylabel('presynaptic axons');
[cx_x,cx_y,cx_ct] = find(cx_AISconnectome);
cx_colorMap = parula(5);
h=scatter(cx_y,cx_x,'.');set(h,'CData',cx_colorMap(min(cx_ct,size(cx_colorMap,1)),:));
set(gca,'Color','Black');

figure
cx_axonsplotted=[];
% for jj=[56 57 58 59 48]
for jj=1:size(cx_SkelID_to_AISid,1)
    
    cx_thisCol = 0.4+rand*0.2;
    aa=cx_allSkelData{i}.plot(cx_SkelID_to_AISid(jj,1),cx_thisCol*[1 1 1]); hold on
    cc=fieldnames(aa);
    bb=getfield(aa,cc{1});
    set(bb,'LineWidth',4);
    
    cx_innervatingAxons = find(cx_AISconnectome(:,jj)>0)
    for kk=1:length(cx_innervatingAxons)
        cx_thisAxonAISfraction = (length(find(cx_allSynData{i}{cx_innervatingAxons(kk)}{2}==1))-1)/(length(find(cx_allSynData{i}{cx_innervatingAxons(kk)}{2}>0))-1);
        cx_theseAISIDs = [cx_allSynData{i}{cx_innervatingAxons(kk)}{5},cx_allSynData{i}{cx_innervatingAxons(kk)}{7}];
        cx_theseAISIDs = cx_theseAISIDs(~isnan(cx_theseAISIDs(:,1)),:);
        cx_theseAIScoords = cx_allSynData{i}{cx_innervatingAxons(kk)}{3}(~isnan(cx_allSynData{i}{cx_innervatingAxons(kk)}{5}),:);
        cx_theseSkelIDs = cx_AISid_to_SkelID(sub2ind(size(cx_AISid_to_SkelID),cx_theseAISIDs(:,1),cx_theseAISIDs(:,2)+1));
        cx_toplot = cx_theseAIScoords(cx_theseSkelIDs==cx_SkelID_to_AISid(jj,1),:);
        if cx_thisAxonAISfraction>0.3
            aa=mplot(cx_toplot,'.m');set(aa,'MarkerSize',20);
            if isempty(find(cx_axonsplotted==cx_axonIds(cx_innervatingAxons(kk))))
                %cx_allSkelData{i}.plot(cx_axonIds(cx_innervatingAxons(kk)),[1 0 1]*rand);
                cx_axonsplotted=[cx_axonsplotted;cx_axonIds(cx_innervatingAxons(kk))];
            end
        else
            aa=mplot(cx_toplot,'.b');set(aa,'MarkerSize',20);
            if isempty(find(cx_axonsplotted==cx_axonIds(cx_innervatingAxons(kk))))
                %cx_allSkelData{i}.plot(cx_axonIds(cx_innervatingAxons(kk)),[0 0 1]);
                cx_axonsplotted=[cx_axonsplotted;cx_axonIds(cx_innervatingAxons(kk))];
            end
        end
        
    end
end
daspect([1 1 1]);    
    
%% Load in final P14 wK data

 cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'
%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

% cx_skelobj = skeleton(fullfile(cx_path,'P14 L2_3 annotations updated_retry.nml'));  % groupIDs working in mac compiled version only
% save(fullfile(cx_path,'P14 L2_3 annotations updated_retry.mat'),'cx_skelobj');

load(fullfile(cx_path,'P14 L2_3 annotations updated_retry.mat'));

cx_AISids = find(cx_skelobj.groupId==1)
cx_axonsCompl = find(cx_skelobj.groupId==3) % note different groupIDs than P28
cx_axonsJustMapping = find(cx_skelobj.groupId==2)


%% Analysis of AIS seeded axons, definition of Axax vs other P14
 cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'
%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

cx_allSkelData = {};
cx_allSkelComments = {};
load(fullfile(cx_path,'P14 L2_3 annotations updated_retry.mat'),'cx_skelobj');
% swap groupIDs to match P28 data
cx_skelobj.groupId(cx_skelobj.groupId==3)=4;
cx_skelobj.groupId(cx_skelobj.groupId==2)=3;
cx_skelobj.groupId(cx_skelobj.groupId==4)=2;

cx_allSkelData{2} = cx_skelobj;
[comments, treeIdx, nodeIdx] = cx_skelobj.getAllComments;
cx_allSkelComments{2} = {comments,treeIdx,nodeIdx};


i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);    % 1: AIS, 2: partial axons for mapping; 3: complete axons
cx_axonSynData = [];
for jj=1:length(cx_axonIds)
    cx_theseCommentLocs = find(cx_allSkelComments{i}{2}==cx_axonIds(jj));
    cx_theseComments = cx_allSkelComments{i}{1}(cx_theseCommentLocs);
    cx_theseCommentCoords = cx_allSkelData{i}.nodes{cx_axonIds(jj)}(cx_allSkelComments{i}{3}(cx_theseCommentLocs),1:3).*...
        repmat(cx_allSkelData{2}.scale,length(cx_theseCommentLocs),1);
    cx_commentTypes=[];cx_synGrade=repmat(0,length(cx_theseCommentLocs),1);cx_AISnum=cx_synGrade*nan;cx_seedLabel=cx_AISnum;
    cx_AIS_nonSomFlag = cx_AISnum;
    for kk=1:length(cx_theseCommentLocs)
        if strfind(cx_theseComments{kk},'syn')
            if strfind(cx_theseComments{kk},'IS')
                cx_commentTypes(kk)=1;
                cx_IDstr= regexp(cx_theseComments{kk},'AIS[\w\-\_]*','match');
                cx_IDstr = regexp(cx_IDstr,'[0-9]+','match');
                if ~isempty(cx_IDstr)
                    if ~isempty(cx_IDstr{1})
                        cx_AISnum(kk) = str2num(cx_IDstr{1}{1});
                    end
                end
                if strfind(cx_theseComments{kk},'NonSom')
                    cx_AIS_nonSomFlag(kk)=1;
                else
                    cx_AIS_nonSomFlag(kk)=0;
                end
            elseif strfind(cx_theseComments{kk},'shaft')
                cx_commentTypes(kk)=3;
            elseif strfind(cx_theseComments{kk},'spin')
                cx_commentTypes(kk)=4;
            else
                cx_commentTypes(kk)=2;
            end
            if strfind(cx_theseComments{kk},'CL1')
                cx_synGrade(kk)=1;
            elseif strfind(cx_theseComments{kk},'CL2')
                cx_synGrade(kk)=2;
            end
            if strfind(cx_theseComments{kk},'seed')
                cx_seedLabel(kk) = 1;
            end
        else
            cx_commentTypes(kk)=0;
        end
    end
    cx_axonSynData(jj,1) = length(find(cx_commentTypes>0)); % n syn
    cx_axonSynData(jj,2) = length(find(cx_commentTypes==1)); % n AIS syn
    cx_axonSynData(jj,3) = cx_allSkelData{i}.pathLength(cx_axonIds(jj));
    if cx_axonSynData(jj,2)/cx_axonSynData(jj,1)>0.3    % error checking: all non-AIS synapses of likely axax axons require synapse grading
        if length(find(cx_synGrade>0))<cx_axonSynData(jj,1)-cx_axonSynData(jj,2)
            sprintf('problem %d %d',i,jj)
        end
    end
    cx_axonSynData(jj,4) = length(find(cx_synGrade(cx_commentTypes~=2)==1));
    cx_axonSynData(jj,5) = length(find(cx_synGrade(cx_commentTypes~=2)==2));
    
    cx_allSynData{i}{jj} = {cx_theseComments,cx_commentTypes,cx_theseCommentCoords,...
        cx_synGrade,cx_AISnum,cx_seedLabel,cx_AIS_nonSomFlag};
    
end

% AIS ID extraction
cx_AIS_ids = find(cx_allSkelData{i}.groupId==1);    % 1: AIS, 2: partial axons for mapping; 3: complete axons
cx_SkelID_to_AISid=[];
for jj=1:length(cx_AIS_ids)
    cx_IDstr= regexp(cx_allSkelData{i}.names{cx_AIS_ids(jj)},'AIS[\w\-\_]*','match');
    cx_IDstr = regexp(cx_IDstr,'[0-9]+','match');
    if ~isempty(cx_IDstr)
        if ~isempty(cx_IDstr{1})
            cx_IDnum = str2num(cx_IDstr{1}{1});
        end
    end
    
    cx_SkelID_to_AISid(jj,1) = cx_AIS_ids(jj);
    cx_SkelID_to_AISid(jj,2) = cx_IDnum;    
    if strfind(cx_allSkelData{i}.names{cx_AIS_ids(jj)},'NonSom')
        cx_SkelID_to_AISid(jj,3)=1;
    else
        cx_SkelID_to_AISid(jj,3)=0;
    end
end

cx_nSynSel = cx_axonSynData(:,1)>=7;
figure
subplot(1,2,1)
plot(cx_axonSynData(cx_nSynSel,1)./cx_axonSynData(cx_nSynSel,3),(cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),'x');hold on
%plot(cx_axonSynData(~cx_nSynSel,1)./cx_axonSynData(~cx_nSynSel,3),(cx_axonSynData(~cx_nSynSel,2)-1)./(cx_axonSynData(~cx_nSynSel,1)-1),'o');hold on

cx_plot=[];
cx_nSynSel_Ids = find(cx_nSynSel);
for kk=1:length(cx_nSynSel_Ids)
   jj=cx_nSynSel_Ids(kk);
   cx_plot(1) =  (cx_axonSynData(jj,2)-1)./(cx_axonSynData(jj,1)-1);
   cx_plot(2) = (cx_axonSynData(jj,2)-1)./(cx_axonSynData(jj,1)-1-cx_axonSynData(jj,5));    % without class 2 synapses
   plot([1 1]*cx_axonSynData(jj,1)./cx_axonSynData(jj,3),cx_plot,'x-k','MarkerSize',15);
end
title('P14, at least 7 syn');
set(gca,'XScal','log');
box off; set(gca,'TickDir','out');set(gca,'YLim',cx_axilim);



subplot(1,2,2)
%histogram((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
hist((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
view([90 90])
set(gca,'XDir','reverse');set(gca,'XLim',cx_axilim);
box off; set(gca,'TickDir','out');

% cluster analysis
cx_clustData = [cx_axonSynData(cx_nSynSel_Ids,1)./cx_axonSynData(cx_nSynSel_Ids,3),(cx_axonSynData(cx_nSynSel_Ids,2)-1)./(cx_axonSynData(cx_nSynSel_Ids,1)-1)];
cx_clustAssgn = clusterdata(cx_clustData,'distance','Euclidean','linkage','ward','maxclust',2);
figure
mplot(cx_clustData,'x'); hold on
set(gca,'YLim',cx_axilim);set(gca,'XScal','log');
mplot(cx_clustData(cx_clustAssgn==1,:),'xr'); hold on

% p(AIS|AIS) min max P14

sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1))
sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5))    % without class 2 synapses


cx_sumdata(1)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1));
cx_fractions1P14=(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1));
cx_sumdata(2)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5));    % without class 2 synapses
cx_fractions1P14_cl1 = (cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5));
cx_sumdata(3)= std(cx_fractions1P14)/sqrt(length(cx_fractions1P14));
cx_sumdata(4)= std(cx_fractions1P14_cl1)/sqrt(length(cx_fractions1P14_cl1));


cx_sumdata(5)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./sum((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1));
cx_fractions2P14=(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1));
cx_sumdata(6)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),5));    % without class 2 synapses
cx_fractions2P14_cl1 = (cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),2)-1)./(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),5));
cx_sumdata(7)= std(cx_fractions2P14)/sqrt(length(cx_fractions2P14));
cx_sumdata(8)= std(cx_fractions2P14_cl1)/sqrt(length(cx_fractions2P14_cl1));

title(sprintf('AxAx %.2f+-%.2f, n=%d; no CL2: %.2f+-%.2f \nNon-axax %.2f+-%.2f, n=%d no CL2: %.2f+-%.2f',cx_sumdata(1)*100,cx_sumdata(3)*100,length(cx_fractions1P14),...
    cx_sumdata(2)*100,cx_sumdata(4)*100,...
    cx_sumdata(5)*100,cx_sumdata(7)*100,length(cx_fractions2P14),...
    cx_sumdata(6)*100,cx_sumdata(8)*100));

[h,p] = kstest2(cx_fractions1P14,cx_fractions1P28)
[h,p] = kstest2(cx_fractions2P14,cx_fractions2P28)

[h,p] = kstest2(cx_fractions1P14_cl1,cx_fractions1P28_cl1)

cx_syndens1P14 = cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)./cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3)*1000;
cx_sumdataDens(1) = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1))/sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3))*1000;
cx_sumdataDens(2) = std(cx_syndens1P14)/sqrt(length(cx_syndens1P14))
cx_sumdataDens(3) = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1))/sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),3))*1000;
cx_syndens2P14 = cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),1)./cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),3)*1000;
cx_sumdataDens(4) = std(cx_syndens2P14)/sqrt(length(cx_syndens2P14))
cx_totalL1 = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3))/1000/1000;
cx_totalL2 = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==2),3))/1000/1000;
figure
subplot(2,1,1),histogram(cx_syndens1P14);hold on;set(gca,'XLim',[0 0.5]);
title(sprintf('P14 syn dens AxAx %.3f +-%.3f per µm, L_{ax} %.2fmm; n=%d',cx_sumdataDens(1:2),cx_totalL1,length(cx_syndens1P14)));
subplot(2,1,2),histogram(cx_syndens2P14);hold on;set(gca,'XLim',[0 0.5]);
title(sprintf('Non-AxAx %.3f +-%.3f per µm, L_{ax} %.2fmm; n=%d',...
    cx_sumdataDens(3:4),cx_totalL2,length(cx_syndens2P14)));

[h,p] = ranksum(cx_syndens1P14,cx_syndens1P28)
[h,p] = ranksum(cx_syndens2P14,cx_syndens2P28)


%% axon plotting with AIS target IDs, based on previous section P14
cx_savepath = fullfile(cx_path,'AxAxGallery');
cx_showAISrecs=1;
i=2
cx_plottype=1;
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);
    %find(cx_axonIds==135) display axon. jj=80
    %find(cx_axonIds==132) display axon. jj=77
for jj=80%77%1:length(cx_axonIds)
    figure
    cx_allSkelData{i}.plot(cx_axonIds(jj));hold on
    daspect([1 1 1]);
%     mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}==1,:),'mo');
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}==1,:),'m.'); set(aa,'MarkerSize',20);
    mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}>1,:),'ko');   
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{4}==1,:),'k.');   set(aa,'MarkerSize',20);
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{4}==2,:),'k.');   set(aa,'MarkerSize',10);
    cx_seedComments = find(cx_allSynData{i}{jj}{6}==1);
    for jjj=1:length(cx_seedComments)
        text(cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),1)-1000,cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),2),cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),3),'S');
    end
    cx_AISids = find(~isnan(cx_allSynData{i}{jj}{5}));
    for jjj=1:length(cx_AISids)
        if cx_allSynData{i}{jj}{7}(cx_AISids(jjj))==1   %non-Soma AIS ID
            text(cx_allSynData{i}{jj}{3}(cx_AISids(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_AISids(jjj),2),cx_allSynData{i}{jj}{3}(cx_AISids(jjj),3),sprintf('n%d',cx_allSynData{i}{jj}{5}(cx_AISids(jjj))),'FontSize',6);
        else
            text(cx_allSynData{i}{jj}{3}(cx_AISids(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_AISids(jjj),2),cx_allSynData{i}{jj}{3}(cx_AISids(jjj),3),sprintf('%d',cx_allSynData{i}{jj}{5}(cx_AISids(jjj))),'FontSize',6);
        end
    end
%     cx_class = find(cx_allSynData{i}{jj}{4}>0);
%     for jjj=1:length(cx_class)
%         text(cx_allSynData{i}{jj}{3}(cx_class(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_class(jjj),2),cx_allSynData{i}{jj}{3}(cx_class(jjj),3),sprintf('cl%d',cx_allSynData{i}{jj}{4}(cx_class(jjj))));        
%     end
%     
    set(gca,'XLim',[0 12]*10^4,'YLim',[0 12]*10^4);
    if cx_plottype==1
        view([90 90]);        set(gca,'XDir','reverse');        xlabel('<WM Pia>')
    elseif cx_plottype==2
        ylabel('<WM Pia>')
    end
    if cx_showAISrecs
        cx_thisAxonTargetAIS = [cx_allSynData{i}{jj}{5},cx_allSynData{i}{jj}{7}];
        cx_thisAxonTargetAIS = unique(cx_thisAxonTargetAIS(~isnan(cx_thisAxonTargetAIS(:,1)),:),'rows');
        for kkk=1:size(cx_thisAxonTargetAIS,1)
            cx_found = find((cx_SkelID_to_AISid(:,2)==cx_thisAxonTargetAIS(kkk,1))&(cx_SkelID_to_AISid(:,3)==cx_thisAxonTargetAIS(kkk,2)));
            cx_thisCol = 0.4+rand*0.2;
            cx_thisSkelID = cx_SkelID_to_AISid(cx_found,1);
            aa=cx_allSkelData{i}.plot(cx_thisSkelID,cx_thisCol*[1 1 1]);
            cc=fieldnames(aa);
            bb=getfield(aa,cc{1});
            set(bb,'LineWidth',2);                
        end        
    end
    title(sprintf('axon %d, P14, %s',cx_axonIds(jj),cx_allSkelData{i}.names{cx_axonIds(jj)}));
%     saveas(gcf,fullfile(cx_savepath,sprintf('P14_axon%d.png',cx_axonIds(jj))));
%     close(gcf);
end
    

%% AIS input mapping, based on the previous section P14
% completely mapped according to AG (email July 29): AIS001, 002, 003, 004,
% 018
% corresponding to rows 56,57,58,59,48 in cx_AISid_to_SkelID

cx_AISid_to_SkelID = full(sparse(cx_SkelID_to_AISid(:,2),cx_SkelID_to_AISid(:,3)+1,cx_SkelID_to_AISid(:,1)));
% assemble ax-to-AIS connectome
i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);
cx_AISconnectome = sparse(length(cx_axonIds),size(cx_SkelID_to_AISid,1));
for jj=1:length(cx_axonIds)
    cx_theseAISIDs = [cx_allSynData{i}{jj}{5},cx_allSynData{i}{jj}{7}];
    cx_theseAISIDs = cx_theseAISIDs(~isnan(cx_theseAISIDs(:,1)),:);
    cx_theseSkelIDs = cx_AISid_to_SkelID(sub2ind(size(cx_AISid_to_SkelID),cx_theseAISIDs(:,1),cx_theseAISIDs(:,2)+1));
    
    cx_thisCxPiece = sparse(jj,cx_theseSkelIDs,1,length(cx_axonIds),size(cx_SkelID_to_AISid,1));
    cx_AISconnectome = cx_AISconnectome + cx_thisCxPiece;    
end
figure
%spy(cx_AISconnectome);
xlabel('AIS');ylabel('presynaptic axons');
[cx_x,cx_y,cx_ct] = find(cx_AISconnectome);
cx_colorMap = parula(5);
h=scatter(cx_y,cx_x,'.');set(h,'CData',cx_colorMap(min(cx_ct,size(cx_colorMap,1)),:));
set(gca,'Color','Black');

figure
cx_axonsplotted=[];
% for jj=[56 57 58 59 48]
for jj=1:size(cx_SkelID_to_AISid,1)
    
    cx_thisCol = 0.4+rand*0.2;
    aa=cx_allSkelData{i}.plot(cx_SkelID_to_AISid(jj,1),cx_thisCol*[1 1 1]); hold on
    cc=fieldnames(aa);
    bb=getfield(aa,cc{1});
    set(bb,'LineWidth',4);
    
    cx_innervatingAxons = find(cx_AISconnectome(:,jj)>0)
    for kk=1:length(cx_innervatingAxons)
        cx_thisAxonAISfraction = (length(find(cx_allSynData{i}{cx_innervatingAxons(kk)}{2}==1))-1)/(length(find(cx_allSynData{i}{cx_innervatingAxons(kk)}{2}>0))-1);
        cx_theseAISIDs = [cx_allSynData{i}{cx_innervatingAxons(kk)}{5},cx_allSynData{i}{cx_innervatingAxons(kk)}{7}];
        cx_theseAISIDs = cx_theseAISIDs(~isnan(cx_theseAISIDs(:,1)),:);
        cx_theseAIScoords = cx_allSynData{i}{cx_innervatingAxons(kk)}{3}(~isnan(cx_allSynData{i}{cx_innervatingAxons(kk)}{5}),:);
        cx_theseSkelIDs = cx_AISid_to_SkelID(sub2ind(size(cx_AISid_to_SkelID),cx_theseAISIDs(:,1),cx_theseAISIDs(:,2)+1));
        cx_toplot = cx_theseAIScoords(cx_theseSkelIDs==cx_SkelID_to_AISid(jj,1),:);
        if cx_thisAxonAISfraction>0.3
            aa=mplot(cx_toplot,'.m');set(aa,'MarkerSize',20);
            if isempty(find(cx_axonsplotted==cx_axonIds(cx_innervatingAxons(kk))))
                %cx_allSkelData{i}.plot(cx_axonIds(cx_innervatingAxons(kk)),[1 0 1]*rand);
                cx_axonsplotted=[cx_axonsplotted;cx_axonIds(cx_innervatingAxons(kk))];
            end
        else
            aa=mplot(cx_toplot,'.b');set(aa,'MarkerSize',20);
            if isempty(find(cx_axonsplotted==cx_axonIds(cx_innervatingAxons(kk))))
                %cx_allSkelData{i}.plot(cx_axonIds(cx_innervatingAxons(kk)),[0 0 1]);
                cx_axonsplotted=[cx_axonsplotted;cx_axonIds(cx_innervatingAxons(kk))];
            end
        end
        
    end
end
daspect([1 1 1]);    
    

%% Load in final P9 wK data

 cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'
%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

% cx_skelobj = skeleton(fullfile(cx_path,'P9 L2_3 AG annotations wi Syn Class.nml'));  % groupIDs working in mac compiled version only
% save(fullfile(cx_path,'P9 L2_3 AG annotations wi Syn Class.mat'),'cx_skelobj');

load(fullfile(cx_path,'P9 L2_3 AG annotations wi Syn Class.mat'));

cx_AISids = find(cx_skelobj.groupId==2)
cx_axonsCompl = find(cx_skelobj.groupId==1) % note different groupIDs than P28
% cx_axonsJustMapping = find(cx_skelobj.groupId==2)


%% Analysis of AIS seeded axons, definition of Axax vs other P9
% cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'
%cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

cx_allSkelData = {};
cx_allSkelComments = {};
load(fullfile(cx_path,'P9 L2_3 AG annotations wi Syn Class.mat'),'cx_skelobj');
% swap groupIDs to match P28 data
cx_skelobj.groupId(cx_skelobj.groupId==1)=3;
cx_skelobj.groupId(cx_skelobj.groupId==2)=1;


cx_allSkelData{2} = cx_skelobj;
[comments, treeIdx, nodeIdx] = cx_skelobj.getAllComments;
cx_allSkelComments{2} = {comments,treeIdx,nodeIdx};


i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);    % 1: AIS, 2: partial axons for mapping; 3: complete axons
cx_axonSynData = [];
for jj=1:length(cx_axonIds)
    cx_theseCommentLocs = find(cx_allSkelComments{i}{2}==cx_axonIds(jj));
    cx_theseComments = cx_allSkelComments{i}{1}(cx_theseCommentLocs);
    cx_theseCommentCoords = cx_allSkelData{i}.nodes{cx_axonIds(jj)}(cx_allSkelComments{i}{3}(cx_theseCommentLocs),1:3).*...
        repmat(cx_allSkelData{2}.scale,length(cx_theseCommentLocs),1);
    cx_commentTypes=[];cx_synGrade=repmat(0,length(cx_theseCommentLocs),1);cx_AISnum=cx_synGrade*nan;cx_seedLabel=cx_AISnum;
    cx_AIS_nonSomFlag = cx_AISnum;
    for kk=1:length(cx_theseCommentLocs)
        if strfind(cx_theseComments{kk},'syn')
            if strfind(cx_theseComments{kk},'IS')
                cx_commentTypes(kk)=1;
                cx_IDstr= regexp(cx_theseComments{kk},'AIS[\w\-\_]*','match');
                cx_IDstr = regexp(cx_IDstr,'[0-9]+','match');
                if ~isempty(cx_IDstr)
                    if ~isempty(cx_IDstr{1})
                        cx_AISnum(kk) = str2num(cx_IDstr{1}{1});
                    end
                end
                if strfind(cx_theseComments{kk},'NonSom')
                    cx_AIS_nonSomFlag(kk)=1;
                else
                    cx_AIS_nonSomFlag(kk)=0;
                end
            elseif strfind(cx_theseComments{kk},'shaft')
                cx_commentTypes(kk)=3;
            elseif strfind(cx_theseComments{kk},'spin')
                cx_commentTypes(kk)=4;
            else
                cx_commentTypes(kk)=2;
            end
            if strfind(cx_theseComments{kk},'CL1')
                cx_synGrade(kk)=1;
            elseif strfind(cx_theseComments{kk},'CL2')
                cx_synGrade(kk)=2;
            end
            if strfind(cx_theseComments{kk},'seed')
                cx_seedLabel(kk) = 1;
            end
        else
            cx_commentTypes(kk)=0;
        end
    end
    cx_axonSynData(jj,1) = length(find(cx_commentTypes>0)); % n syn
    cx_axonSynData(jj,2) = length(find(cx_commentTypes==1)); % n AIS syn
    cx_axonSynData(jj,3) = cx_allSkelData{i}.pathLength(cx_axonIds(jj));
    if cx_axonSynData(jj,2)/cx_axonSynData(jj,1)>0.3    % error checking: all non-AIS synapses of likely axax axons require synapse grading
        if length(find(cx_synGrade>0))<cx_axonSynData(jj,1)-cx_axonSynData(jj,2)
            sprintf('problem %d %d',i,jj)
        end
    end
    cx_axonSynData(jj,4) = length(find(cx_synGrade(cx_commentTypes~=2)==1));
    cx_axonSynData(jj,5) = length(find(cx_synGrade(cx_commentTypes~=2)==2));
    
    cx_allSynData{i}{jj} = {cx_theseComments,cx_commentTypes,cx_theseCommentCoords,...
        cx_synGrade,cx_AISnum,cx_seedLabel,cx_AIS_nonSomFlag};
    
end

% AIS ID extraction
cx_AIS_ids = find(cx_allSkelData{i}.groupId==1);    % 1: AIS, 2: partial axons for mapping; 3: complete axons
cx_SkelID_to_AISid=[];
for jj=1:length(cx_AIS_ids)
    cx_IDstr= regexp(cx_allSkelData{i}.names{cx_AIS_ids(jj)},'AIS[\w\-\_]*','match');
    cx_IDstr = regexp(cx_IDstr,'[0-9]+','match');
    if ~isempty(cx_IDstr)
        if ~isempty(cx_IDstr{1})
            cx_IDnum = str2num(cx_IDstr{1}{1});
        end
    end
    
    cx_SkelID_to_AISid(jj,1) = cx_AIS_ids(jj);
    cx_SkelID_to_AISid(jj,2) = cx_IDnum;    
    if strfind(cx_allSkelData{i}.names{cx_AIS_ids(jj)},'NonSom')
        cx_SkelID_to_AISid(jj,3)=1;
    else
        cx_SkelID_to_AISid(jj,3)=0;
    end
end

cx_nSynSel = cx_axonSynData(:,1)>=7;
figure
subplot(1,2,1)
plot(cx_axonSynData(cx_nSynSel,1)./cx_axonSynData(cx_nSynSel,3),(cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),'x');hold on
%plot(cx_axonSynData(~cx_nSynSel,1)./cx_axonSynData(~cx_nSynSel,3),(cx_axonSynData(~cx_nSynSel,2)-1)./(cx_axonSynData(~cx_nSynSel,1)-1),'o');hold on

cx_plot=[];
cx_nSynSel_Ids = find(cx_nSynSel);
for kk=1:length(cx_nSynSel_Ids)
   jj=cx_nSynSel_Ids(kk);
   cx_plot(1) =  (cx_axonSynData(jj,2)-1)./(cx_axonSynData(jj,1)-1);
   cx_plot(2) = (cx_axonSynData(jj,2)-1)./(cx_axonSynData(jj,1)-1-cx_axonSynData(jj,5));    % without class 2 synapses
   plot([1 1]*cx_axonSynData(jj,1)./cx_axonSynData(jj,3),cx_plot,'x-k','MarkerSize',15);
end
title('P9, at least 7 syn');
set(gca,'XScal','log');set(gca,'YLim',cx_axilim);box off; set(gca,'TickDir','out');

subplot(1,2,2)
%histogram((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
hist((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
view([90 90])
set(gca,'XDir','reverse');set(gca,'XLim',cx_axilim);
box off; set(gca,'TickDir','out');

cx_sumdataDens=[]

cx_clustAssgn = repmat(1,1,length(cx_nSynSel_Ids));
cx_sumdata(1)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1));
cx_fractions1P9=(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./((cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1));
cx_sumdata(2)=sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5));    % without class 2 synapses
cx_fractions1P9_cl1 = (cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),2)-1)./(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)-1-cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),5));
cx_sumdata(3)= std(cx_fractions1P9)/sqrt(length(cx_fractions1P9));
cx_sumdata(4)= std(cx_fractions1P9_cl1)/sqrt(length(cx_fractions1P9_cl1));

cx_syndens1P9 = cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1)./cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3)*1000;
cx_sumdataDens(1) = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),1))/sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3))*1000;
cx_sumdataDens(2) = std(cx_syndens1P9)/sqrt(length(cx_syndens1P9))
cx_totalL = sum(cx_axonSynData(cx_nSynSel_Ids(cx_clustAssgn==1),3))/1000/1000;
title(sprintf('all %.2f+-%.2f, n=%d; no CL2: %.2f+-%.2f\nsyn dens all %.3f +-%.3f per µm, L_{ax} %.2fmm',cx_sumdata(1)*100,cx_sumdata(3)*100,length(cx_fractions1P9),...
    cx_sumdata(2)*100,cx_sumdata(4)*100,...
    cx_sumdataDens,cx_totalL));


%% axon plotting with AIS target IDs, based on previous section P9
cx_savepath = fullfile(cx_path,'AxAxGallery');
cx_showAISrecs=1;
i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);
for jj=14%1:length(cx_axonIds)
    figure
    cx_allSkelData{i}.plot(cx_axonIds(jj));hold on
    daspect([1 1 1]);
%     mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}==1,:),'mo');
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}==1,:),'m.'); set(aa,'MarkerSize',20);
    mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{2}>1,:),'ko');   
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{4}==1,:),'k.');   set(aa,'MarkerSize',20);
    aa=mplot(cx_allSynData{i}{jj}{3}(cx_allSynData{i}{jj}{4}==2,:),'k.');   set(aa,'MarkerSize',10);
    cx_seedComments = find(cx_allSynData{i}{jj}{6}==1);
    for jjj=1:length(cx_seedComments)
        text(cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),1)-1000,cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),2),cx_allSynData{i}{jj}{3}(cx_seedComments(jjj),3),'S');
    end
    cx_AISids = find(~isnan(cx_allSynData{i}{jj}{5}));
    for jjj=1:length(cx_AISids)
        if cx_allSynData{i}{jj}{7}(cx_AISids(jjj))==1   %non-Soma AIS ID
            text(cx_allSynData{i}{jj}{3}(cx_AISids(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_AISids(jjj),2),cx_allSynData{i}{jj}{3}(cx_AISids(jjj),3),sprintf('n%d',cx_allSynData{i}{jj}{5}(cx_AISids(jjj))),'FontSize',6);
        else
            text(cx_allSynData{i}{jj}{3}(cx_AISids(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_AISids(jjj),2),cx_allSynData{i}{jj}{3}(cx_AISids(jjj),3),sprintf('%d',cx_allSynData{i}{jj}{5}(cx_AISids(jjj))),'FontSize',6);
        end
    end
%     cx_class = find(cx_allSynData{i}{jj}{4}>0);
%     for jjj=1:length(cx_class)
%         text(cx_allSynData{i}{jj}{3}(cx_class(jjj),1)+500,cx_allSynData{i}{jj}{3}(cx_class(jjj),2),cx_allSynData{i}{jj}{3}(cx_class(jjj),3),sprintf('cl%d',cx_allSynData{i}{jj}{4}(cx_class(jjj))));        
%     end
%     
    set(gca,'XLim',[0 12]*10^4,'YLim',[0 12]*10^4);
    if i==1
        view([90 90]);        set(gca,'XDir','reverse');        xlabel('<WM Pia>')
    elseif i==2
        ylabel('<WM Pia>')
    end
    if cx_showAISrecs
        cx_thisAxonTargetAIS = [cx_allSynData{i}{jj}{5},cx_allSynData{i}{jj}{7}];
        cx_thisAxonTargetAIS = unique(cx_thisAxonTargetAIS(~isnan(cx_thisAxonTargetAIS(:,1)),:),'rows');
        for kkk=1:size(cx_thisAxonTargetAIS,1)
            cx_found = find((cx_SkelID_to_AISid(:,2)==cx_thisAxonTargetAIS(kkk,1))&(cx_SkelID_to_AISid(:,3)==cx_thisAxonTargetAIS(kkk,2)));
            cx_thisCol = 0.4+rand*0.2;
            cx_thisSkelID = cx_SkelID_to_AISid(cx_found,1);
            aa=cx_allSkelData{i}.plot(cx_thisSkelID,cx_thisCol*[1 1 1]);
            cc=fieldnames(aa);
            bb=getfield(aa,cc{1});
            set(bb,'LineWidth',2);                
        end        
    end
    %saveas(gcf,fullfile(cx_savepath,sprintf('P28_axon%d.png',cx_axonIds(jj))));
    %close(gcf);
end
    

%% AIS input mapping, based on the previous section P9
% completely mapped according to AG (email July 29): AIS001, 002, 003, 004,
% 018
% corresponding to rows 56,57,58,59,48 in cx_AISid_to_SkelID

cx_AISid_to_SkelID = full(sparse(cx_SkelID_to_AISid(:,2),cx_SkelID_to_AISid(:,3)+1,cx_SkelID_to_AISid(:,1)));
% assemble ax-to-AIS connectome
i=2
cx_axonIds = find(cx_allSkelData{i}.groupId>=2);
cx_AISconnectome = sparse(length(cx_axonIds),size(cx_SkelID_to_AISid,1));
for jj=1:length(cx_axonIds)
    cx_theseAISIDs = [cx_allSynData{i}{jj}{5},cx_allSynData{i}{jj}{7}];
    cx_theseAISIDs = cx_theseAISIDs(~isnan(cx_theseAISIDs(:,1)),:);
    cx_theseSkelIDs = cx_AISid_to_SkelID(sub2ind(size(cx_AISid_to_SkelID),cx_theseAISIDs(:,1),cx_theseAISIDs(:,2)+1));
    
    cx_thisCxPiece = sparse(jj,cx_theseSkelIDs,1,length(cx_axonIds),size(cx_SkelID_to_AISid,1));
    cx_AISconnectome = cx_AISconnectome + cx_thisCxPiece;    
end
figure
%spy(cx_AISconnectome);
xlabel('AIS');ylabel('presynaptic axons');
[cx_x,cx_y,cx_ct] = find(cx_AISconnectome);
cx_colorMap = parula(5);
h=scatter(cx_y,cx_x,'.');set(h,'CData',cx_colorMap(min(cx_ct,size(cx_colorMap,1)),:));
set(gca,'Color','Black');

figure
cx_axonsplotted=[];
% for jj=[56 57 58 59 48]
for jj=1:size(cx_SkelID_to_AISid,1)
    
    cx_thisCol = 0.4+rand*0.2;
    aa=cx_allSkelData{i}.plot(cx_SkelID_to_AISid(jj,1),cx_thisCol*[1 1 1]); hold on
    cc=fieldnames(aa);
    bb=getfield(aa,cc{1});
    set(bb,'LineWidth',4);
    
    cx_innervatingAxons = find(cx_AISconnectome(:,jj)>0)
    for kk=1:length(cx_innervatingAxons)
        cx_thisAxonAISfraction = (length(find(cx_allSynData{i}{cx_innervatingAxons(kk)}{2}==1))-1)/(length(find(cx_allSynData{i}{cx_innervatingAxons(kk)}{2}>0))-1);
        cx_theseAISIDs = [cx_allSynData{i}{cx_innervatingAxons(kk)}{5},cx_allSynData{i}{cx_innervatingAxons(kk)}{7}];
        cx_theseAISIDs = cx_theseAISIDs(~isnan(cx_theseAISIDs(:,1)),:);
        cx_theseAIScoords = cx_allSynData{i}{cx_innervatingAxons(kk)}{3}(~isnan(cx_allSynData{i}{cx_innervatingAxons(kk)}{5}),:);
        cx_theseSkelIDs = cx_AISid_to_SkelID(sub2ind(size(cx_AISid_to_SkelID),cx_theseAISIDs(:,1),cx_theseAISIDs(:,2)+1));
        cx_toplot = cx_theseAIScoords(cx_theseSkelIDs==cx_SkelID_to_AISid(jj,1),:);
        if cx_thisAxonAISfraction>0.3
            aa=mplot(cx_toplot,'.m');set(aa,'MarkerSize',20);
            if isempty(find(cx_axonsplotted==cx_axonIds(cx_innervatingAxons(kk))))
                %cx_allSkelData{i}.plot(cx_axonIds(cx_innervatingAxons(kk)),[1 0 1]*rand);
                cx_axonsplotted=[cx_axonsplotted;cx_axonIds(cx_innervatingAxons(kk))];
            end
        else
            aa=mplot(cx_toplot,'.b');set(aa,'MarkerSize',20);
            if isempty(find(cx_axonsplotted==cx_axonIds(cx_innervatingAxons(kk))))
                %cx_allSkelData{i}.plot(cx_axonIds(cx_innervatingAxons(kk)),[0 0 1]);
                cx_axonsplotted=[cx_axonsplotted;cx_axonIds(cx_innervatingAxons(kk))];
            end
        end
        
    end
end
daspect([1 1 1]);    
    
%% P56 AxAx analysis from xls file

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
cx_syndata = xlsread(fullfile(cx_path,'L23_RawData_16-07-2020_forCartridgeAnalysis_P56update.xlsx'));


cx_sel = [166:169,171:181];
cx_fractions = (cx_syndata(cx_sel,8)-1)./(cx_syndata(cx_sel,6)-1)
cx_syndens = cx_syndata(cx_sel,6)./cx_syndata(cx_sel,4);
cx_nsynsel = find(cx_syndata(cx_sel,6)>=7)

figure
subplot(1,2,1)
plot(cx_syndens(cx_nsynsel),cx_fractions(cx_nsynsel),'x','MarkerSize',15);
title('P56, at least 7 syn');
set(gca,'XScal','log');set(gca,'YLim',cx_axilim);box off; set(gca,'TickDir','out');
subplot(1,2,2)
%histogram((cx_axonSynData(cx_nSynSel,2)-1)./(cx_axonSynData(cx_nSynSel,1)-1),[0:0.05:1])
hist(cx_fractions(cx_nsynsel),[0:0.05:1])
view([90 90])
set(gca,'XDir','reverse');set(gca,'XLim',cx_axilim);
box off; set(gca,'TickDir','out');


%% AxAx cartridge addition model results

% cartridge splitting data in p28_chand_cartridges_pl_NH_KS_v2_evalMH.xlsx
% (data from NH and KS, double-checked between them).

% updated table: AxAx_P14P28_cartridgeaddition_NH_KSmeas.xlsx   - with
% ranges of cartridge definition

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_data = xlsread(fullfile(cx_path,'AxAx_P14P28_cartridgeaddition_NH_KSmeas.xlsx'));
cx_range = [1:9]
cx_totalPL = sum(cx_data(cx_range,1))
cx_totalPLcart_min = sum(cx_data(cx_range,4))
cx_totalPLcart_max = sum(cx_data(cx_range,5))

cx_cartPerPLminmax = [cx_totalPLcart_min cx_totalPLcart_max]./(cx_totalPL-[cx_totalPLcart_min cx_totalPLcart_max])
% 0.1562    0.2361

% from mhcode_devCon_part4.m section %% CHANDELIER - AXAX analysis with
% proper synapse criteria:
cx_bulkP14 = [0.0262    0.0199]
cx_bulkP28 = [0.0449    0.0047]
cx_AISsynperCartridgeL = 0.24 % from Main Table

cx_predictedInn=[];
%min:
cx_predictedInn(1) = (cx_bulkP14(1) + cx_cartPerPLminmax(1)*cx_AISsynperCartridgeL)/(cx_bulkP14(1) + cx_cartPerPLminmax(1)*cx_AISsynperCartridgeL + cx_bulkP14(2))
%max:
cx_predictedInn(2) = (cx_bulkP14(1) + cx_cartPerPLminmax(2)*cx_AISsynperCartridgeL)/(cx_bulkP14(1) + cx_cartPerPLminmax(2)*cx_AISsynperCartridgeL + cx_bulkP14(2))

% cx_predictedInn= 0.7619    0.8063

%Required pruning fraction of off-target syn:
cx_measInn = 0.9005 % main table: p(ais|ais) for ax-ax axons

cx_KeepFrac = (cx_bulkP14(1) + cx_cartPerPLminmax(1)*cx_AISsynperCartridgeL)*(1/cx_measInn-1)/ cx_bulkP14(2)
cx_PrunFrac = 1-cx_KeepFrac

cx_KeepFrac = (cx_bulkP14(1) + cx_cartPerPLminmax(2)*cx_AISsynperCartridgeL)*(1/cx_measInn-1)/ cx_bulkP14(2)
cx_PrunFrac = 1-cx_KeepFrac

% ergo 54-65% pruning needed on top.

figure
subplot(1,2,1),bar(1-[cx_predictedInn,cx_measInn]);box off; set(gca,'TickDir','out');
subplot(1,2,2),bar([cx_predictedInn,cx_measInn]);;box off; set(gca,'TickDir','out');

% now with ranges: p(AIS|AIS) is 0.8936 .. 0.9515 at P28 (see section
% above)
% at P14 it's 0.5698 ("weaker" synapses in processes of removal, so take all syns at P14)

figure
plot(14,[0.57],'x');hold on
plot([1 1]*28,[0.89 0.95],'x-');hold on
plot([1 1]*28,cx_predictedInn,'x-');hold on
set(gca,'XLim',[0 30],'TickDir','out','YLim',[0 1]);box off
plot([14 28],[0.57 0.89],'-k');
plot([14 28],[0.57 0.95],'-k');
plot([14 28],[0.57 cx_predictedInn(1)],'-k');
plot([14 28],[0.57 cx_predictedInn(2)],'-k');

%% Re-checking of all data: P7-P9 soma axons L4

% path length check

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'

cx_skelobj_L4d1 = skeleton(fullfile(cx_path,'AG_P9_L4_30-06-2016__explorational__mhelmstaedter__0b0402_P9_L4_d1_sm001_axon003.nml'));

cx_skelobj_L4d3 = skeleton(fullfile(cx_path,'2020-06-06-st048-align__explorational__mhelmstaedter__0b0659_P9_L4_d3_sm006_ax002.nml'));
    
 

    cx_skelobj_L4d1.pathLength
    cx_skelobj_L4d3.pathLength
    
    
    
%% High res analysis

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P7 = skeleton(fullfile(cx_path,'P7_highres_axons_somaSeed_mh.nml'));

cx_skelobj_P9 = skeleton(fullfile(cx_path,'P9_highres_axons_somaSeed_mh.nml'));

    
aa=cx_skelobj_P7.pathLength/1000
bb=cx_skelobj_P7.names

    
cc=cx_skelobj_P9.pathLength/1000
dd=cx_skelobj_P9.names
    
p7_psom = [0.277777778	0.152777778	0.161616162]
p7_synDens = [0.105291738	0.27758731	0.379688389]
p9_psom = [0.230769231	0.22	0.25]
p9_synDens = [0.149611403	0.249352338	0.328313912]

figure
plot(p7_synDens,p7_psom,'xb-');hold on
plot(p9_synDens,p9_psom,'xr-');hold on
set(gca,'XLim',[0 0.5],'YLim',[0 1]);

p7_data_d2_paper = [0.14 0.06] % syn d, psom from Fig. S5
p9_data_d3_paper = [0.1 0.15]
    
mplot(p7_data_d2_paper,'ob');    
mplot(p9_data_d3_paper,'or');
mplot([p7_data_d2_paper;p9_data_d3_paper],'-k');

    
p7_data_d1_paper = [0.16 0.06] % syn d, psom from Fig. S5
p9_data_d12_paper = [0.09 0.15]
    
mplot(p7_data_d1_paper,'ob');    
mplot(p9_data_d12_paper,'or');
mplot([p7_data_d1_paper;p9_data_d12_paper],'-k');
    
xlabel('Syn Density per um');ylabel('Psom');    
    
    
   

%% reannotation by authors: Path length readout

% path length readout P9 L4 d1

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P9_reann = skeleton(fullfile(cx_path,'P9_L4_d1_reannotations_skeletonsForPathlength.nml'));

aa=cx_skelobj_P9_reann.names;
bb=cx_skelobj_P9_reann.pathLength/10^3;


% path length readout P7 L4 d1

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P7_reann = skeleton(fullfile(cx_path,'p7 d1 realigned reannotation by authors.nml'));

cc=cx_skelobj_P7_reann.names;
dd=cx_skelobj_P7_reann.pathLength/10^3;
    
    
% path length readout P7 L4 d2 high res part 2

cx_skelobj_P7_highres = skeleton(fullfile(cx_path,'2020-08-25-st060-align-v4__explorational__mhelmstaedter__3f9b7f_highresAnn_fromwK.nml'));

eec=cx_skelobj_P7_highres.names;
ff=cx_skelobj_P7_highres.pathLength/10^3;

% path length readout P9 L4 d3 high res

cx_skelobj_P9_highres = skeleton(fullfile(cx_path,'p9 highres d3 annotations.nml'));

gg=cx_skelobj_P9_highres.names;
hh=cx_skelobj_P9_highres.pathLength/10^3;

% path length readout P7 L4 d2 low res reannotation

cx_skelobj_P7_lowres = skeleton(fullfile(cx_path,'P7 n=2 lowres reannotation_download.nml'));

ii=cx_skelobj_P7_lowres.names;
jj=cx_skelobj_P7_lowres.pathLength/10^3;

% path length readout P9 L4 d3 low res reannotation

cx_skelobj_P9_lowres = skeleton(fullfile(cx_path,'p9 d3 lowres reannotation_wKdownload.nml'));

kk=cx_skelobj_P9_lowres.names;
ll=cx_skelobj_P9_lowres.pathLength/10^3;

% path length readout P7 L4 d1 low res reannotation AD seeded axons

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P7_reann = skeleton(fullfile(cx_path,'p7 d1 realigned reannotation by authors_ADseededAxons.nml'));

aa=cx_skelobj_P7_reann.names;
bb=cx_skelobj_P7_reann.pathLength/10^3;


% path length readout P9 L4 d1 low res reannotation AD seeded axons

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
%cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P7_reann = skeleton(fullfile(cx_path,'P9-d1 reannotation by authors_ADaxons_forPlength.nml'));

aa=cx_skelobj_P7_reann.names;
bb=cx_skelobj_P7_reann.pathLength/10^3;



%% check local synapse densities in soma axons P9 L4 d1

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P9_AG = skeleton(fullfile(cx_path,'P9-d1 L4 for skeleton download.nml'));

% determine (soma) syn nodes

[comments, treeIdx, nodeIdx] = cx_skelobj_P9_AG.getAllComments;

cx_allSynData={};
cx_axonIds = [1:length(cx_skelobj_P9_AG.nodes)]
cx_axonSynData = [];
for jj=1:length(cx_axonIds)
    cx_theseCommentLocs = find(treeIdx==cx_axonIds(jj));
    cx_theseComments = comments(cx_theseCommentLocs);
    cx_theseCommentCoords =cx_skelobj_P9_AG.nodes{cx_axonIds(jj)}(nodeIdx(cx_theseCommentLocs),1:3).*...
        repmat(cx_skelobj_P9_AG.scale,length(cx_theseCommentLocs),1);
    cx_theseCommentNodeIdx = nodeIdx(cx_theseCommentLocs);
    cx_commentTypes=[];%cx_synGrade=repmat(0,length(cx_theseCommentLocs),1);cx_AISnum=cx_synGrade*nan;cx_seedLabel=cx_AISnum;
    %cx_AIS_nonSomFlag = cx_AISnum;
    for kk=1:length(cx_theseCommentLocs)
        if strfind(cx_theseComments{kk},'syn')
            if strfind(cx_theseComments{kk},'som')
                cx_commentTypes(kk)=1;              
            elseif strfind(cx_theseComments{kk},'Som')
                cx_commentTypes(kk)=1;              
            elseif strfind(cx_theseComments{kk},'shaft')
                cx_commentTypes(kk)=3;
            elseif strfind(cx_theseComments{kk},'spin')
                cx_commentTypes(kk)=4;
            else
                cx_commentTypes(kk)=2;
            end            
        else
            cx_commentTypes(kk)=0;
        end
    end
    cx_axonSynData(jj,1) = length(find(cx_commentTypes>0)); % n syn
    cx_axonSynData(jj,2) = length(find(cx_commentTypes==1)); % n som syn
    cx_axonSynData(jj,3) = cx_skelobj_P9_AG.pathLength(cx_axonIds(jj));
    
    cx_allSynData{jj} = {cx_theseComments,cx_commentTypes,cx_theseCommentCoords,cx_theseCommentNodeIdx};
    
end

cx_axsel = find(cx_axonSynData(:,1)>0)
%%cx_skelobj_P9_AG.splitCC
cx_bboxSizes = [10 20 30 50 100 1000]
cx_plotType=2 % 1 syn dens, 2=som frac
cx_restrSamplingData = {};
figure
for mm=1:length(cx_bboxSizes)
    cx_bboxSizeMicrons = [1 1 1]*cx_bboxSizes(mm);
    subplot(2,3,mm)
    for jj=1:length(cx_axsel)
        cx_skelID=cx_axsel(jj);
        cx_theseSomaSynNodeIdx = cx_allSynData{cx_skelID}{4}(cx_allSynData{cx_skelID}{2}==1);
        for kk=1:length(cx_theseSomaSynNodeIdx)
            cx_thiscenterNodeIdx=cx_theseSomaSynNodeIdx(kk);
            cx_bbox_vx = ceil(cx_bboxSizeMicrons./cx_skelobj_P9_AG.scale*1000/2);
            cx_bbox_vx = [-cx_bbox_vx;cx_bbox_vx]'+repmat(cx_skelobj_P9_AG.nodes{cx_skelID}(cx_thiscenterNodeIdx,1:3)',[1 2])
            cx_theseValidNodes = (sum(cx_skelobj_P9_AG.nodes{cx_skelID}(:,1:3)>=repmat(cx_bbox_vx(:,1)',size(cx_skelobj_P9_AG.nodes{cx_skelID},1),1),2)==3).*...
                (sum(cx_skelobj_P9_AG.nodes{cx_skelID}(:,1:3)<=repmat(cx_bbox_vx(:,2)',size(cx_skelobj_P9_AG.nodes{cx_skelID},1),1),2)==3);
            cx_theseValidEdges = sum(cx_theseValidNodes(cx_skelobj_P9_AG.edges{cx_skelID}),2)==2;
            cx_thisGraph = graph(cx_skelobj_P9_AG.edges{cx_skelID}(cx_theseValidEdges,1),cx_skelobj_P9_AG.edges{cx_skelID}(cx_theseValidEdges,2));
            cx_thisccomp = conncomp(cx_thisGraph);
            cx_thisccomp(size(cx_skelobj_P9_AG.nodes{cx_skelID},1))=0; % fill until max node id
            cx_thisccompID = cx_thisccomp(cx_thiscenterNodeIdx);
            cx_nodesInBBoxAndCcomp = cx_thisccomp==cx_thisccompID;
            cx_edgesInBBoxAndCcomp = sum(cx_nodesInBBoxAndCcomp(cx_skelobj_P9_AG.edges{cx_skelID}),2)==2;
            cx_coordsNm = cx_skelobj_P9_AG.nodes{cx_skelID}(:,1:3).*repmat(cx_skelobj_P9_AG.scale,size(cx_skelobj_P9_AG.nodes{cx_skelID},1),1);
            cx_pLengthInBboxAndCcomp = sum((sum((cx_coordsNm(cx_skelobj_P9_AG.edges{cx_skelID}(cx_edgesInBBoxAndCcomp,2),:) - ...
                cx_coordsNm(cx_skelobj_P9_AG.edges{cx_skelID}(cx_edgesInBBoxAndCcomp,1),:)).^2,2)).^(1/2))/1000;
            cx_nSynInRestr = sum(cx_nodesInBBoxAndCcomp(cx_allSynData{cx_skelID}{4}(cx_allSynData{cx_skelID}{2}>0)));
            cx_nSynInRestr_som = sum(cx_nodesInBBoxAndCcomp(cx_allSynData{cx_skelID}{4}(cx_allSynData{cx_skelID}{2}==1)));
            
            cx_restrSamplingData{jj}(kk,1)=cx_pLengthInBboxAndCcomp;
            cx_restrSamplingData{jj}(kk,2)=cx_nSynInRestr;
            cx_restrSamplingData{jj}(kk,3)=cx_thiscenterNodeIdx;
            cx_restrSamplingData{jj}(kk,4)=cx_nSynInRestr_som;
            
        end
        if cx_plotType ==1 % syn dens
            plot(jj,cx_restrSamplingData{jj}(:,2)./cx_restrSamplingData{jj}(:,1),'xk');hold on
            plot(jj,sum(cx_allSynData{cx_skelID}{2}>0)/cx_skelobj_P9_AG.pathLength(cx_skelID)*1000,'or','MarkerSize',10);hold on
        else% som frac
            plot(jj,(cx_restrSamplingData{jj}(:,4)-1)./(cx_restrSamplingData{jj}(:,2)-1),'xk');hold on
            plot(jj,(sum(cx_allSynData{cx_skelID}{2}==1)-1)/(sum(cx_allSynData{cx_skelID}{2}>0)-1),'or','MarkerSize',10);hold on
        end
        
    end
    title(sprintf('bbox %.0f um',cx_bboxSizes(mm)));
    if cx_plotType ==1 % syn dens
        set(gca,'YLim',[0 0.5]);
        ylabel('synapse density (per um)');
    else
        set(gca,'YLim',[0 0.5]);
        ylabel('soma reinnervation fraction');
    end
end


%% check local synapse densities in soma axons P7 L4 d1

cx_path = 'X:\Personal\mh\_mhlab\publications\2016 DevelopmentalConnectomics\2020 REVISION SCIENCE\'
cx_path = '/Users/mh/Documents/_mhlab/publications/2016 DevelopmentalConnectomics/2020 REVISION SCIENCE'

cx_skelobj_P9_AG = skeleton(fullfile(cx_path,'P7-d1 L4 for skeleton download.nml'));    % VAR NAME KEPT BUT ITS P7!!

% determine (soma) syn nodes

[comments, treeIdx, nodeIdx] = cx_skelobj_P9_AG.getAllComments;

cx_allSynData={};
cx_axonIds = [1:length(cx_skelobj_P9_AG.nodes)]
cx_axonSynData = [];
for jj=1:length(cx_axonIds)
    cx_theseCommentLocs = find(treeIdx==cx_axonIds(jj));
    cx_theseComments = comments(cx_theseCommentLocs);
    cx_theseCommentCoords =cx_skelobj_P9_AG.nodes{cx_axonIds(jj)}(nodeIdx(cx_theseCommentLocs),1:3).*...
        repmat(cx_skelobj_P9_AG.scale,length(cx_theseCommentLocs),1);
    cx_theseCommentNodeIdx = nodeIdx(cx_theseCommentLocs);
    cx_commentTypes=[];%cx_synGrade=repmat(0,length(cx_theseCommentLocs),1);cx_AISnum=cx_synGrade*nan;cx_seedLabel=cx_AISnum;
    %cx_AIS_nonSomFlag = cx_AISnum;
    for kk=1:length(cx_theseCommentLocs)
        if strfind(cx_theseComments{kk},'syn')
            if strfind(cx_theseComments{kk},'som')
                cx_commentTypes(kk)=1;              
            elseif strfind(cx_theseComments{kk},'Som')
                cx_commentTypes(kk)=1;              
            elseif strfind(cx_theseComments{kk},'shaft')
                cx_commentTypes(kk)=3;
            elseif strfind(cx_theseComments{kk},'spin')
                cx_commentTypes(kk)=4;
            else
                cx_commentTypes(kk)=2;
            end            
        else
            cx_commentTypes(kk)=0;
        end
    end
    cx_axonSynData(jj,1) = length(find(cx_commentTypes>0)); % n syn
    cx_axonSynData(jj,2) = length(find(cx_commentTypes==1)); % n som syn
    cx_axonSynData(jj,3) = cx_skelobj_P9_AG.pathLength(cx_axonIds(jj));
    
    cx_allSynData{jj} = {cx_theseComments,cx_commentTypes,cx_theseCommentCoords,cx_theseCommentNodeIdx};
    
end

cx_axsel = find(cx_axonSynData(:,1)>0)
%%cx_skelobj_P9_AG.splitCC
cx_bboxSizes = [10 20 30 50 100 1000]
cx_plotType=2 % 1 syn dens, 2=som frac
cx_restrSamplingData = {};
figure
for mm=1:length(cx_bboxSizes)
    cx_bboxSizeMicrons = [1 1 1]*cx_bboxSizes(mm);
    subplot(2,3,mm)
    for jj=1:length(cx_axsel)
        cx_skelID=cx_axsel(jj);
        cx_theseSomaSynNodeIdx = cx_allSynData{cx_skelID}{4}(cx_allSynData{cx_skelID}{2}==1);
        for kk=1:length(cx_theseSomaSynNodeIdx)
            cx_thiscenterNodeIdx=cx_theseSomaSynNodeIdx(kk);
            cx_bbox_vx = ceil(cx_bboxSizeMicrons./cx_skelobj_P9_AG.scale*1000/2);
            cx_bbox_vx = [-cx_bbox_vx;cx_bbox_vx]'+repmat(cx_skelobj_P9_AG.nodes{cx_skelID}(cx_thiscenterNodeIdx,1:3)',[1 2])
            cx_theseValidNodes = (sum(cx_skelobj_P9_AG.nodes{cx_skelID}(:,1:3)>=repmat(cx_bbox_vx(:,1)',size(cx_skelobj_P9_AG.nodes{cx_skelID},1),1),2)==3).*...
                (sum(cx_skelobj_P9_AG.nodes{cx_skelID}(:,1:3)<=repmat(cx_bbox_vx(:,2)',size(cx_skelobj_P9_AG.nodes{cx_skelID},1),1),2)==3);
            cx_theseValidEdges = sum(cx_theseValidNodes(cx_skelobj_P9_AG.edges{cx_skelID}),2)==2;
            cx_thisGraph = graph(cx_skelobj_P9_AG.edges{cx_skelID}(cx_theseValidEdges,1),cx_skelobj_P9_AG.edges{cx_skelID}(cx_theseValidEdges,2));
            cx_thisccomp = conncomp(cx_thisGraph);
            cx_thisccomp(size(cx_skelobj_P9_AG.nodes{cx_skelID},1))=0; % fill until max node id
            cx_thisccompID = cx_thisccomp(cx_thiscenterNodeIdx);
            cx_nodesInBBoxAndCcomp = cx_thisccomp==cx_thisccompID;
            cx_edgesInBBoxAndCcomp = sum(cx_nodesInBBoxAndCcomp(cx_skelobj_P9_AG.edges{cx_skelID}),2)==2;
            cx_coordsNm = cx_skelobj_P9_AG.nodes{cx_skelID}(:,1:3).*repmat(cx_skelobj_P9_AG.scale,size(cx_skelobj_P9_AG.nodes{cx_skelID},1),1);
            cx_pLengthInBboxAndCcomp = sum((sum((cx_coordsNm(cx_skelobj_P9_AG.edges{cx_skelID}(cx_edgesInBBoxAndCcomp,2),:) - ...
                cx_coordsNm(cx_skelobj_P9_AG.edges{cx_skelID}(cx_edgesInBBoxAndCcomp,1),:)).^2,2)).^(1/2))/1000;
            cx_nSynInRestr = sum(cx_nodesInBBoxAndCcomp(cx_allSynData{cx_skelID}{4}(cx_allSynData{cx_skelID}{2}>0)));
            cx_nSynInRestr_som = sum(cx_nodesInBBoxAndCcomp(cx_allSynData{cx_skelID}{4}(cx_allSynData{cx_skelID}{2}==1)));
            
            cx_restrSamplingData{jj}(kk,1)=cx_pLengthInBboxAndCcomp;
            cx_restrSamplingData{jj}(kk,2)=cx_nSynInRestr;
            cx_restrSamplingData{jj}(kk,3)=cx_thiscenterNodeIdx;
            cx_restrSamplingData{jj}(kk,4)=cx_nSynInRestr_som;
            
        end
        if cx_plotType ==1 % syn dens
            plot(jj,cx_restrSamplingData{jj}(:,2)./cx_restrSamplingData{jj}(:,1),'xk');hold on
            plot(jj,sum(cx_allSynData{cx_skelID}{2}>0)/cx_skelobj_P9_AG.pathLength(cx_skelID)*1000,'or','MarkerSize',10);hold on
        else% som frac
            plot(jj,(cx_restrSamplingData{jj}(:,4)-1)./(cx_restrSamplingData{jj}(:,2)-1),'xk');hold on
            plot(jj,(sum(cx_allSynData{cx_skelID}{2}==1)-1)/(sum(cx_allSynData{cx_skelID}{2}>0)-1),'or','MarkerSize',10);hold on
        end
        
    end
    title(sprintf('bbox %.0f um',cx_bboxSizes(mm)));
    if cx_plotType ==1 % syn dens
        set(gca,'YLim',[0 0.5]);
        ylabel('synapse density (per um)');
    else
        set(gca,'YLim',[0 0.5]);
        ylabel('soma reinnervation fraction');
    end
end










