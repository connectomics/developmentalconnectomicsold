%% FIGURE_05 - L2/3 PLOTS
Age = [9 14 28];

%% L2/3 AIS_input_synapse density
cx_data = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L23_RawData_04-07-2020.xlsx','Sheet2');
ToSel_1 = find(cx_data(:,1)== 9); 
pL9= cx_data(ToSel_1,3);
tSyn9 = cx_data(ToSel_1,9);
P9 = tSyn9./pL9;

ToSel_2 = find(cx_data(:,1)== 14); 
pL14= cx_data(ToSel_2,3);
tSyn14 = cx_data(ToSel_2,9);
P14 = tSyn14./pL14;

ToSel_3 = find(cx_data(:,1)== 28); 
pL28= cx_data(ToSel_3,3);
tSyn28 = cx_data(ToSel_3,9);
P28 = tSyn28./pL28;

AIS = cat(1,P9,P14,P28);
Err_9 = std(P9)/sqrt(numel(P9));
Err_14 = std(P14)/sqrt(numel(P14));
Err_28 = std(P28)/sqrt(numel(P28));
Err = cat(1,Err_9,Err_14,Err_28);
Mean = [mean(P9);mean(P14);mean(P28)];
g1=ones(size(P9)); g2 =2*ones(size(P14)); g3=3*ones(size(P28));
G=cat(1,g1,g2,g3);

%Plotting
figure
boxplot(AIS,G,'Positions',[9 14 28],'Colors','k')
hold on
plot([9 14 28],Mean,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot([9 14 28], (Mean+Err),'--k')
hold on
plot([9 14 28],Mean-Err,'--k')
hold on
scatter(ones(numel(P9),1)*9+rand(numel(P9),1)*.75-.45, P9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P14),1)*14+rand(numel(P14),1)*.75-.45, P14,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P28),1)*28+rand(numel(P28),1)*.75-.45, P28,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
box off
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 9 14 28])
set(gca,'XtickLabel',[0 9 14 28])
set(gca,'TickDir','out')
xlabel('Postnatal ages (days)')
ylabel('Input synapses on AIS (per unit um AIS length')
title('L2/3 - AIS input synapse density')

%{
%% Fractional innervation vs SynDensity plots
clear all
cx_data = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L23_RawData_04-07-2020.xlsx','Sheet1');
%P9 OtherAxons
ToSel_1 = find(cx_data(:,1)== 9& cx_data(:,3)== 2); 
AISInn_9 = (cx_data(ToSel_1,8)-1)./(cx_data(ToSel_1,6)-1);
AISDens_9 = cx_data(ToSel_1,6)./cx_data(ToSel_1,4);
BulkInn9 = sum((cx_data(ToSel_1,8)-1))/sum((cx_data(ToSel_1,6)-1));
BulkDens9 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

%P14AxAx axons
ToSel_2 = find(cx_data(:,1)== 14& cx_data(:,3)== 2.1);%2.1 for Ax-Ax axons 
AISInn_14AX = (cx_data(ToSel_2,8)-1)./(cx_data(ToSel_2,6)-1);
AISDens_14AX = cx_data(ToSel_2,6)./cx_data(ToSel_2,4);
BulkInn14AX = sum((cx_data(ToSel_2,8)-1))/sum((cx_data(ToSel_2,6)-1));
BulkDens14AX = sum(cx_data(ToSel_2,6))/sum(cx_data(ToSel_2,4));

%P14OTHER axons
ToSel_3 = find(cx_data(:,1)== 14& cx_data(:,3)== 2.2);%2.2 for Other axons 
AISInn_14 = (cx_data(ToSel_3,8)-1)./(cx_data(ToSel_3,6)-1);
AISDens_14 = cx_data(ToSel_3,6)./cx_data(ToSel_3,4);
BulkInn14 = sum((cx_data(ToSel_3,8)-1))/sum((cx_data(ToSel_3,6)-1));
BulkDens14 = sum(cx_data(ToSel_3,6))/sum(cx_data(ToSel_3,4));

%P28AxAx axons
ToSel_4 = find(cx_data(:,1)== 28& cx_data(:,3)== 2.1);%2.1 for Ax-Ax axons 
AISInn_28AX = (cx_data(ToSel_4,8)-1)./(cx_data(ToSel_4,6)-1);
AISDens_28AX = cx_data(ToSel_4,6)./cx_data(ToSel_4,4);
BulkInn28AX = sum((cx_data(ToSel_4,8)-1))/sum((cx_data(ToSel_4,6)-1));
BulkDens28AX = sum(cx_data(ToSel_4,6))/sum(cx_data(ToSel_4,4));
 
%P28OTHER axons
ToSel_5 = find(cx_data(:,1)== 28& cx_data(:,3)== 2.2);%2.2 for Other axons 
AISInn_28 = (cx_data(ToSel_5,8)-1)./(cx_data(ToSel_5,6)-1);
AISDens_28 = cx_data(ToSel_5,6)./cx_data(ToSel_5,4);
BulkInn28 = sum((cx_data(ToSel_5,8)-1))/sum((cx_data(ToSel_5,6)-1));
BulkDens28 = sum(cx_data(ToSel_5,6))/sum(cx_data(ToSel_5,4));

BulkInn = cat(1,BulkInn9,BulkInn14,BulkInn28);
BulkInnAX = cat(1,BulkInn14AX,BulkInn28AX);
BulkDens = cat(1,BulkDens9,BulkDens14,BulkDens28);
BulkDensAX = cat(1,BulkDens14AX,BulkDens28AX);

SemErrInn = cat(1,std(AISInn_9)/sqrt(numel(AISInn_9)),...
std(AISInn_14)/sqrt(numel(AISInn_14)),...
std(AISInn_28)/sqrt(numel(AISInn_28)));

SemErrInnAX = cat(1,std(AISInn_14AX)/sqrt(numel(AISInn_14AX)),...
std(AISInn_28AX)/sqrt(numel(AISInn_28AX)));

SemErrDens = cat(1,std(AISDens_9)/sqrt(numel(AISDens_9)),...
std(AISDens_14)/sqrt(numel(AISDens_14)),...
std(AISDens_28)/sqrt(numel(AISDens_28)));

SemErrDensAX = cat(1,std(AISDens_14AX)/sqrt(numel(AISDens_14AX)),...
std(AISDens_28AX)/sqrt(numel(AISDens_28AX)));

%scatter plot
figure
hold on
subplot(1,3,1)
scatter(AISDens_9,AISInn_9,'xb','SizeData',100)
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 0.5])
set(gca,'Ytick',[0 0.2 0.4 0.6 0.8 1])
box off;set(gca,'TickDir','out')
ylabel('Fractional innervation of AIS')
xlabel('Synapse density (syn/um axonal path length)')
title('P9')
daspect([1 1 1])

subplot(1,3,2)
hold on
scatter(AISDens_14,AISInn_14,'xb','SizeData',100)
scatter(AISDens_14AX,AISInn_14AX,'xm','SizeData',100)
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 0.5])
set(gca,'Ytick',[0 0.2 0.4 0.6 0.8 1])
box off;set(gca,'TickDir','out')
ylabel('Fractional innervation of AIS')
xlabel('Synapse density (syn/um axonal path length)')
title('P14')
daspect([1 1 1])

subplot(1,3,3)
hold on
scatter(AISDens_28,AISInn_28,'xb','SizeData',100)
scatter(AISDens_28AX,AISInn_28AX,'xm','SizeData',100)
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 0.5])
set(gca,'Ytick',[0 0.2 0.4 0.6 0.8 1])
box off;set(gca,'TickDir','out')
ylabel('Fractional innervation of AIS')
xlabel('Synapse density (syn/um axonal path length)')
title('P28')
daspect([1 1 1])


%% Fraction of AIS input synapses by AxAx and other axons

cx_data = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L23_RawData_04-07-2020.xlsx','Sheet2');
ToSel_1 = find(cx_data(:,1)== 9); 
P9nAxAx= cx_data(ToSel_1,4); BulkP9nAxAx = sum(cx_data(ToSel_1,4))/numel(cx_data(ToSel_1,4)); SE_P9nAxAx = std(P9nAxAx)/sqrt(numel(P9nAxAx));
P9nOtAx= cx_data(ToSel_1,5); BulkP9nOtAx = sum(cx_data(ToSel_1,5))/numel(cx_data(ToSel_1,5)); SE_P9nOtAx = std(P9nOtAx)/sqrt(numel(P9nOtAx));


ToSel_2 = find(cx_data(:,1)== 14); 
P14Ax= cx_data(ToSel_2,7)./ cx_data(ToSel_2,9); BulkP14Ax = sum(cx_data(ToSel_2,7))/sum(cx_data(ToSel_2,9));
P14Ot= cx_data(ToSel_2,8)./ cx_data(ToSel_2,9); BulkP14Ot = sum(cx_data(ToSel_2,8))/sum(cx_data(ToSel_2,9));
P14nAxAx= cx_data(ToSel_2,4); BulkP14nAxAx = sum(cx_data(ToSel_2,4))/numel(cx_data(ToSel_2,4)); SE_P14nAxAx = std(P14nAxAx)/sqrt(numel(P14nAxAx));
P14nOtAx= cx_data(ToSel_2,5); BulkP14nOtAx = sum(cx_data(ToSel_2,5))/numel(cx_data(ToSel_2,5)); SE_P14nOtAx = std(P14nOtAx)/sqrt(numel(P14nOtAx));


ToSel_3 = find(cx_data(:,1)== 28); 
P28Ax= cx_data(ToSel_3,7)./ cx_data(ToSel_3,9);BulkP28Ax = sum(cx_data(ToSel_3,7))/sum(cx_data(ToSel_3,9));
P28Ot= cx_data(ToSel_3,8)./ cx_data(ToSel_3,9);BulkP28Ot= sum(cx_data(ToSel_3,8))/sum(cx_data(ToSel_3,9));
P28nAxAx= cx_data(ToSel_3,4); BulkP28nAxAx = sum(cx_data(ToSel_3,4))/numel(cx_data(ToSel_3,4)); SE_P28nAxAx = std(P28nAxAx)/sqrt(numel(P28nAxAx));
P28nOtAx= cx_data(ToSel_3,5); BulkP28nOtAx = sum(cx_data(ToSel_3,5))/numel(cx_data(ToSel_3,5)); SE_P28nOtAx = std(P28nOtAx)/sqrt(numel(P28nOtAx));

BulknAxAx = cat(1,BulkP9nAxAx,BulkP14nAxAx,BulkP28nAxAx);
BulknOtAx = cat(1,BulkP9nOtAx,BulkP14nOtAx,BulkP28nOtAx);
SE_nAxAx = cat(1,SE_P9nAxAx,SE_P14nAxAx,SE_P28nAxAx);
SE_nOtAx = cat(1,SE_P9nOtAx,SE_P14nOtAx,SE_P28nOtAx);

AISAx = cat(1,P14Ax,P28Ax);
AISOt = cat(1,P14Ot,P28Ot);


Err_14Ax = std(P14Ax)/sqrt(numel(P14Ax));
Err_14Ot = std(P14Ot)/sqrt(numel(P14Ot));
Err_28Ax = std(P28Ax)/sqrt(numel(P28Ax));
Err_28Ot = std(P28Ot)/sqrt(numel(P28Ot));
ErrAx = cat(1,Err_14Ax,Err_28Ax);
ErrOt = cat(1,Err_14Ot,Err_28Ot);

BulkAx = cat(1,BulkP14Ax,BulkP28Ax);
BulkOt = cat(1,BulkP14Ot,BulkP28Ot);
g2 =2*ones(size(P14Ot)); g3=3*ones(size(P28Ot));
G=cat(1,g2,g3);

%Plotting
figure
hold on
boxplot(AISOt,G,'Positions',[1 2],'Colors','k')
boxplot(AISAx,G,'Positions',[1 2],'Colors','k')
plot([1 2],BulkAx,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot([1 2],BulkOt,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot([1 2],BulkAx+ErrAx,'--m')
plot([1 2],BulkAx-ErrAx,'--m')
plot([1 2],BulkOt+ErrOt,'--b')
plot([1 2],BulkOt-ErrOt,'--b')
scatter(ones(numel(P14Ax),1)*1+rand(numel(P14Ax),1)*.25-.125, P14Ax,'x','MarkerEdgeColor','m','SizeData',150);
scatter(ones(numel(P28Ax),1)*2+rand(numel(P28Ax),1)*.25-.125, P28Ax,'x','MarkerEdgeColor','m','SizeData',150);
scatter(ones(numel(P14Ot),1)*1+rand(numel(P14Ot),1)*.25-.125, P14Ot,'x','MarkerEdgeColor','b','SizeData',150);
scatter(ones(numel(P28Ot),1)*2+rand(numel(P28Ot),1)*.25-.125, P28Ot,'x','MarkerEdgeColor','b','SizeData',150);
box off
set(gca,'Ylim',[0 1])
set(gca,'Xtick',[1 2])
set(gca,'XtickLabel',[14 28])
set(gca,'TickDir','out')
xlabel('Postnatal ages (days)')
ylabel('Fraction of synapses on AIS')
title('L2/3 - Fraction of synapses - AxAx vs Other')

% Cartridge synapse density histogram
skel = skeleton('Z:\Data\goura\FigureUpdates\L23\AxonCartridgeSpecSynDensity\NML\SplitBranches_P28L23_ChAxons_11-11-2019.nml');

treeIndices = find(cellfun(@(x)any(strfind(x, 'Ch')==1),skel.names));
treeNames = skel.names(treeIndices);
SplitAxons = length(treeIndices); %all branches of all axons post splitting
AISSyn = {};
AllSyn = skel.getNodesWithComment('syn',treeIndices,'partial');
AISComments = skel.getNodesWithComment('AIS',treeIndices,'partial');%for soma axons

for i = 1:SplitAxons
  AISSyn{i}= intersect(AllSyn{i},AISComments{i});
end
AISSyn = AISSyn';

AISSyn_num = zeros(SplitAxons,1);
SSsyn_num = zeros(SplitAxons,1);
TotalSyn = zeros(SplitAxons,1);
Specificity = zeros(SplitAxons,1);
SpecSynDens = zeros(SplitAxons,1);
OtherSynDens = zeros(SplitAxons,1);
TotalDens = zeros(SplitAxons,1);

for i = 1:SplitAxons
    AISSyn_num(i) = numel(AISSyn{i});
    SSsyn_num(i) = AISSyn_num(i);
    TotalSyn(i) = numel(AllSyn{i});
    pL(i) = skel.pathLength(treeIndices(i))/1000;
    SpecSynDens(i) = SSsyn_num(i)/pL(i);
end
specDens = histcounts(SpecSynDens,0:0.05:0.5);
BulkCartDens = sum(AISSyn_num)/sum(pL);

figure
stairs((0:0.05:0.5)',[specDens';specDens(end)],'m','LineWidth',2,'LineStyle','-');
hold on
box off
set(gca,'Ylim',[0 10])
set(gca,'Xlim',[0 0.5])
set(gca,'TickDir','out')
xlabel('Synapses per um cartridge length)')
ylabel('Cartridges (P28)')
title('L2/3 - AIS syn density of Cartridges P28')

%%





% PathLengthDensity of AxAx axons

cx_data = xlsread('Z:\Data\goura\FigUpdates_Revision\RawData_NML\L23_RawData_04-07-2020.xlsx','Sheet3');

ToSel_2 = find(cx_data(:,1)== 14);
P14 = cx_data(ToSel_2,3);
ToSel_3 = find(cx_data(:,1)== 28);
P28 = cx_data(ToSel_3,3);

AIS = cat(1,P14,P28);

Err_14 = std(P14)/sqrt(numel(P14));
Err_28 = std(P28)/sqrt(numel(P28));
Err = cat(1,Err_14,Err_28);
Mean = [mean(P14);mean(P28)];
g2 =2*ones(size(P14)); g3=3*ones(size(P28));
G=cat(1,g2,g3);

%Plotting
figure
boxplot(AIS,G,'Colors','k')
hold on
plot(Mean,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot((Mean+Err),'--m')
hold on
plot(Mean-Err,'--m')
hold on
scatter(ones(numel(P14),1)*1+rand(numel(P14),1)*.25-.125, P14,'x','MarkerEdgeColor','m','SizeData',200);
hold on
scatter(ones(numel(P28),1)*2+rand(numel(P28),1)*.25-.125, P28,'x','MarkerEdgeColor','m','SizeData',200);
box off
set(gca,'Ylim',[0 0.01])
set(gca,'Xtick',[1 2])
set(gca,'XtickLabel',[14 28])
set(gca,'TickDir','out')
xlabel('Postnatal ages (days)')
ylabel('Axonal path length density')
title('L2/3 - PathLength density of AxAx axons')

%% 
%Model prediction
ObsInn = [BulkP14AISinn BulkP28AISinn];
dL = (Mean(2,:)-Mean(1,:))/(Mean(1,:));
CartModelPredict = (BulkP14AIS+(dL*BulkCartDens))/(BulkP14AIS+BulkP14Oth+(dL*BulkCartDens));
ModelCart = [BulkP14AISinn CartModelPredict];

PrunThr = (0.1:0.1:1);
PrunPredict = zeros(10,1);
for i=1:10
PrunPredict(i) = (BulkP14AIS+(dL*BulkCartDens))/(BulkP14AIS+((1-PrunThr(i))*BulkP14Oth)+(dL*BulkCartDens));
end

figure
plot([1 2],ObsInn,'x--m');
hold on
plot([1 2],ModelCart,'x--','Color',[0.5 0.5 0.5])
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xtick',[1 2])
set(gca,'XtickLabel',[14 28])
xlabel('Postnatal age (days)')
ylabel('Fractional AIS innervation (%)')

figure
plot((1-PrunPredict),'-b')
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 0.2])
set(gca,'Ytick',[0 0.1 0.2])
set(gca,'Xtick',[0 4 8])
set(gca,'XtickLabel',[0 40 80])
xlabel('Antispecific pruning (% of non-AIS synapses)')
ylabel('Non-AIS innervation (%)')
%}
