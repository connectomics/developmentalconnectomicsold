## About Developmental paper code repository:

This repository contains all the code and data used for the following manuscript:   
**Postnatal connectomic development of inhibition in mouse barrel cortex**   
Anjali Gour, Kevin M. Boergens, Natalie Heike,  Yunfeng Hua, Philip Lasterstein, Kun Song and  Moritz Helmstaedter   
  

## License
This project is licensed under the [MIT license](LICENSE).  
Copyright (c) 2020 Department of Connectomics, Max Planck Institute for
Brain Research, D-60438 Frankfurt am Main, Germany
