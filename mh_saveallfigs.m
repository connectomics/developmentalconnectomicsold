function mh_saveallfigs(m_path,m_type)
    if nargin<2
        m_type='png';
    end
    aa=get(groot,'Children');
    for ii=1:size(aa,1)
        
        saveas(aa(ii).Number,fullfile(m_path,sprintf('matlabfigure%.0f',aa(ii).Number)),m_type);
    end





end